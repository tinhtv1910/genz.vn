<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name') ;
            $table->string('email')->unique();

            $table->string('phone')->nullable();
            $table->string('phone_area')->nullable();

            $table->integer('province_id')->nullable();
            $table->integer('district_id')->nullable();

            $table->integer('ward_id')->nullable();
            $table->string('address')->nullable();
            $table->date('dob')->nullable();

            $table->string('image')->nullable();
            $table->string('ip_adress')->nullable();
            $table->text('user_agent')->nullable();
            $table->text('description')->nullable();

            $table->tinyInteger('role_id')->default(2);
            $table->tinyInteger('status')->default(1);

            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
