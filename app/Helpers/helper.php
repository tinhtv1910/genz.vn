<?php

use App\Models\Sidebar;

if (!function_exists('options')) {
    function options()
    {
        return ['10', '20', '30', '50', '100'];
    }
}

if (!function_exists('sidebars')) {
    function sidebars()
    {
        $sidebars = Sidebar::with('subMenus')->where('parent_id', 0)->orderBy('order', 'asc')->get();

        return $sidebars;
    }
}
