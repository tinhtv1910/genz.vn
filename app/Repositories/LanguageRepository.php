<?php

namespace App\Repositories;

use App\Models\Language;

class LanguageRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Language::class;
    }

    public function multiLanguageStatusUpdate($request) {
        return $this->model->whereIn('id', $request['listUserIds'])->update([
            'status' => $request['status']
        ]);
    }
}
