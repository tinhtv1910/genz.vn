<?php

namespace App\Repositories;

use App\Models\PermissionRole;

class PermissionRoleRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return PermissionRole::class;
    }
}
