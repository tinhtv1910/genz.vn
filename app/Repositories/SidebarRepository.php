<?php

namespace App\Repositories;

use App\Models\Sidebar;

class SidebarRepository extends BaseRepository
{
    public $html = '';
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Sidebar::class;
    }

    public function recursive($id = null, $parent_id = 0, $str = '')
    {

        $sidebars = $this->getAll(['parent_id' => $parent_id]);

        foreach ($sidebars as $sidebar) {
            $selected = $sidebar->id == $id ? "selected" : "";
            if ($sidebar->parent_id == $parent_id && $sidebar->type_id != 1) {
                $this->html .= "<option value='" . $sidebar->id . "' " . $selected . ">" . $str . $sidebar->name . "</option>";
                $this->recursive($id, $sidebar->id, $str . '___');
            }
        }

        return $this->html;
    }
}
