<?php

namespace App\Repositories;

use App\Models\Order;

class OrderRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Order::class;
    }

    public function prepareQuery() {
        return $this->model->with(['orderStatus', 'province', 'district', 'ward']);
    }
}
