<?php

namespace App\Repositories;

use App\Models\Menu;
use App\Models\MenuType;

class MenuRepository extends BaseRepository
{
    public $html = '';
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Menu::class;
    }

    public function recursive($id = null, $parent_id = 0, $str = '')
    {

        $menus = $this->getAll(['parent_id' => $parent_id]);

        foreach ($menus as $menu) {
            $selected = $menu->id == $id ? "selected" : "";
            if ($menu->parent_id == $parent_id) {
                $this->html .= "<option value='" . $menu->id . "' " . $selected . ">" . $str . $menu->name . "</option>";
                $this->recursive($id, $menu->id, $str . '___');
            }
        }

        return $this->html;
    }

    public function getMenuType()
    {
        return MenuType::all();
    }
}
