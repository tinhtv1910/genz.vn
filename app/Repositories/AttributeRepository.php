<?php

namespace App\Repositories;

use App\Models\Attribute;

class AttributeRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Attribute::class;
    }

    public function prepareQuery()
    {
        return $this->model->with('attributeValues');
    }
}
