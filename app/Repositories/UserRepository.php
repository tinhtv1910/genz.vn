<?php

namespace App\Repositories;

use App\Models\Province;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return User::class;
    }

    public function getListProvince()
    {
        return Province::orderBy('order', 'asc')->get();
    }

    public function getListRole()
    {
        $roles = [];
        if (Auth::user()->role_id == 1) {
            $roles = Role::all();
        } else {
            $roles = Role::where('id', '!=', 1)->get();
        }
        return $roles;
    }

    public function multiUserStatusUpdate($request)
    {
        return $this->model->whereIn('id', $request['listUserIds'])->update([
            'status' => $request['status']
        ]);
    }
}
