<?php

namespace App\Repositories;

use App\Models\PostCategory;

class PostCategoryRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return PostCategory::class;
    }
}
