<?php

namespace App\Repositories;

use App\Models\Type;

/**
 * Class TypeRepository.
 */
class TypeRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Type::class;
    }
}
