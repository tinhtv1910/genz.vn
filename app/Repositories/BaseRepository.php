<?php

namespace App\Repositories;

use App\Repositories\Interfaces\BaseRepositoryInterface;
use Exception;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class BaseRepository.
 */
abstract class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    protected $app;

    protected $limit = 25;

    protected $sort = 'id:desc';

    public function __construct(Container $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract public function model();

    private function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new Exception("Class {$this->model()} must be instance of Illuminate\\Database\\Eloquent\\Model");
        }

        $this->model = $model;

        return $this->model;
    }

    public function getAll($params = [], $query = null)
    {
        if (!$query) {
            $query = $this->prepareQuery();
        }

        $filters = Arr::except($params, ['limit', 'sorts']);
        $limit = data_get($params, 'limit', $this->limit);

        // convert string sort to array
        $sorts = explode(',', data_get($params, 'sorts', $this->sort));
        $orders = [];

        foreach ($sorts as $sort) {
            $item = explode(':', $sort);

            // Check sort and assign value
            if (count($item) >= 2) {
                $key = $item[0];
                $val = $item[1];

                $orders[$key] = $val;

                $query = $query->orderBy($key, $val);
            }
        }

        if (!is_numeric($limit)) {
            $limit = 10000000;
        }

        $data = $query
            ->filter($filters)
            ->sort($orders)
            ->paginate($limit)
            ->appends($params);

        $this->resetModel();

        return $data;
    }

    public function find($id, $column = ['*'])
    {
        $query = $this->prepareQuery();

        $data = $query->find($id, $column);

        $this->resetModel();

        return $data;
    }

    public function findOrFail($id, $column = ['*'])
    {
        $query = $this->prepareQuery();

        $data = $query->findOrFail($id, $column);

        $this->resetModel();

        return $data;
    }

    public function create($data = [])
    {
        $query = $this->prepareQuery();

        $object = $query->create($data);

        return $object;
    }

    public function update($data = ['*'], $id)
    {
        $query = $this->prepareQuery();

        $result = $query->findOrFail($id);

        $result->fill($data);

        $result->save();

        return $result;
    }

    public function findByField($filed, $value)
    {
        $query = $this->prepareQuery();

        $object = $query->where($filed, $value)->first();

        $this->resetModel();

        return $object;
    }

    public function destroy($id)
    {
        $query = $this->prepareQuery();

        $result = $query->findOrFail($id);

        return $result->delete();
    }

    public function destroyMultiple($ids)
    {
        return  $this->model->destroy($ids);
    }

    protected function prepareQuery()
    {
        return $this->model->newQuery();
    }

    protected function resetModel()
    {
        $this->makeModel();
    }
}
