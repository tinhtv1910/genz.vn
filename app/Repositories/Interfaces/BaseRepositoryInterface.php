<?php

namespace App\Repositories\Interfaces;

interface BaseRepositoryInterface
{
    public function getAll(array $params=['*'], $query);

    public function find(string $id, array $column = ['*']);

    public function findOrFail(string $id, $column = ['*']);

    // public function findByField($data = [], $column=['*']);

    public function create(array $data);

    public function update(array $data = ['*'], string $id);

    public function destroy(string $id);
}
