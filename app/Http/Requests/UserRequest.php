<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $year = date('Y') - 4;
        $passValidate = $this->id ? 'nullable' : 'bail|required|min:6|max:40';
        $passConfirmValidate = $this->id ? 'nullable' : 'bail|required|min:6|max:40|same:password';

        return [
            'email' => 'bail|required|email:rfc|max:191|unique:users,email,' . $this->id,
            'name' => 'bail|required|min:3|max:100',
            'password' => $passValidate,
            'confirm_password' => $passConfirmValidate,
            'dob' => 'bail|nullable|date|before:' . $year . '-01-01'
        ];
    }

    public function messages()
    {
        return [
            'dob.before' => "Try creating users with an age greater than or equal to 5."
        ];
    }
}
