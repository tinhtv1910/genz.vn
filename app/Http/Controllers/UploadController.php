<?php

namespace App\Http\Controllers;

use App\Traits\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    use UploadImage;

    public function upload(Request $request) {
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');

            $fileHash = Storage::url($this->uploadFile($file, 'image', 'uploads'));

            $response = "<script>window.parent.CKEDITOR.tools.callFunction(".$request->input('CKEditorFuncNum').", '".$fileHash."', '')</script>";
            @header('Content-type: text/html; charset=utf-8');

            echo $response;
        }
    }
}
