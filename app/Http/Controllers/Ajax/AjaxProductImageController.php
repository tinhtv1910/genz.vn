<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\ProductImage;
use App\Traits\UploadImage;
use Illuminate\Http\Request;

class AjaxProductImageController extends Controller
{
    use UploadImage;

    public function destroy($id)
    {
        $productImage = ProductImage::findOrFail($id);

        if ($productImage && $productImage->image) {
            $this->unlinkFile($productImage->image);
        }

        $productImage->delete();

        return $this->successJson();
    }
}
