<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class AjaxPermissionController extends Controller
{
    public $permissionRepository;
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function destroyMultiple(Request $request)
    {
        $this->authorize('module', 'pemissions.all');
        $ids = $request['listUserIds'];

        if (!is_array($ids) || !$ids) return $this->errorJson();

        $this->permissionRepository->destroyMultiple($ids);

        return $this->successJson();
    }
}
