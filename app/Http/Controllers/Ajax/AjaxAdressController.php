<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Ward;
use Illuminate\Http\Request;

class AjaxAdressController extends Controller
{
    public function getAdressById(Request $request)
    {
        if (!$request->get('id') && $request->get('id') == 0) {
            return $this->errorJson(null, 'Not found address ID.');
        }

        $type = $request->get('type') ?? 1;
        $dataId = $request->get('id');

        if ($type == 1) {
            $data = District::with('unit')->where('province_id', $dataId)->get();
        } else {
            $data = Ward::with('unit')->where('district_id', $dataId)->get();
        }

        $html = self::makeAddressHtml($data, $type);
        return $this->successJson($html);
    }

    protected function makeAddressHtml($data, $type = 1)
    {
        $label = $type == 1 ? 'district' : 'ward';
        $html = '<option value="0" selected>--Select ' . $label . '--</option>';
        foreach ($data as $item) {
            $html .= '<option value="' . $item->id . '">' . $item->unit->short_name . ' ' . ucwords($item->name) . '</option>';
        }

        return $html;
    }
}
