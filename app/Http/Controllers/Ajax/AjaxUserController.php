<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class AjaxUserController extends Controller
{
    public $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function updateUserStatus(Request $request)
    {
        if (!$request->get('id')) {
            return $this->errorJson();
        }

        $object = $this->userRepository->update($request->all(), $request->get('id'));

        return $this->successJson(['status' => $object->status]);
    }

    public function multiUserStatusUpdate(Request $request)
    {
        $this->userRepository->multiUserStatusUpdate($request->all());

        return $this->successJson();
    }

    public function destroyMultiple(Request $request)
    {
        $ids = $request['listUserIds'];

        if (!is_array($ids) || !$ids) return $this->errorJson();

        $this->userRepository->destroyMultiple($ids);

        return $this->successJson();
    }
}
