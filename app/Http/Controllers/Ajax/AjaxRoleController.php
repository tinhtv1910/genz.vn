<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class AjaxRoleController extends Controller
{
    public $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function updateRoleStatus(Request $request)
    {
        if (!$request->get('id')) {
            return $this->errorJson();
        }

        $object = $this->roleRepository->update($request->all(), $request->get('id'));

        $this->userBelongToRoleStatus($object);

        return $this->successJson(['status' => $object->status]);
    }

    public function userBelongToRoleStatus($object)
    {
        return User::where('role_id', $object->id)->update([
            'status' => $object->status
        ]);
    }

    public function multiRoleStatusUpdate(Request $request)
    {
    }

    public function destroyMultiple(Request $request)
    {
        // $ids = $request['listUserIds'];

        // if (!is_array($ids) || !$ids) return $this->errorJson();
    }
}
