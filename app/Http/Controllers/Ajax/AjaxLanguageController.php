<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Repositories\LanguageRepository;
use Illuminate\Http\Request;

class AjaxLanguageController extends Controller
{
    public $languageRepository;
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function updateLanguageStatus(Request $request)
    {
        if (!$request->get('id')) {
            return $this->errorJson();
        }

        $object = $this->languageRepository->update($request->all(), $request->get('id'));

        return $this->successJson(['status' => $object->status]);
    }

    public function multiLanguageStatusUpdate(Request $request)
    {
        $this->languageRepository->multiLanguageStatusUpdate($request->all());

        return $this->successJson();
    }

    public function destroyMultiple(Request $request)
    {
        $ids = $request['listUserIds'];

        if (!is_array($ids) || !$ids) return $this->errorJson();

        $this->languageRepository->destroyMultiple($ids);

        return $this->successJson();
    }
}
