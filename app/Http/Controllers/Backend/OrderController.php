<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\OrderStatus;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class OrderController extends Controller
{
    public $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $today = \Illuminate\Support\Carbon::now()->format('Y-m-d');
        $orders = $this->orderRepository->getAll($request->all());

        $data = [
            'data' => $orders,
            'now' => $today,
            'title' => 'Manage orders',
            'singleName' => 'order',
            'prefix' => 'orders',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];

        return view('backend.pages.orders.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $item = $this->orderRepository->findOrFail($id);
        $status = OrderStatus::all();
        $data = [
            'item' => $item,
            'status' => $status,
            'title' => 'Show order',
            'prefix' => 'orders',
            'parentBreadcrumb' => "Manage orders",
        ];

        return view('backend.pages.orders.show', $data);
    }

    public function showInvoice($id)
    {
        $item = $this->orderRepository->findOrFail($id);
        $data = ['item' => $item];
        return view('backend.pages.orders.invoice', $data);
    }

    public function downloadInvoice($id)
    {
        $item = $this->orderRepository->findOrFail($id);
        $data = ['item' => $item];
        $pdf = Pdf::loadView('backend.pages.orders.invoice', $data);
        return $pdf
            ->setOption(['defaultFont' => 'DejaVu Sans'])
            ->setPaper('a4', 'landscape')
            ->download('heji_invoice_' . $item->id . '_' . date('dmYsiH') . '.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $order = $this->orderRepository->findOrFail($id);
        if ($order && $order->status != 6) {
            $this->orderRepository->update($request->only('status'), $id);
            return $this->redirectBack('msg', 'Order status updated successfully.');
        }
        return $this->redirectBack('err', 'Order already transport success can not update.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
