<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Models\Role;
use App\Models\User;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('module', 'roles.index');
        $relation = Role::with('users');
        $roles = $this->roleRepository->getAll($request->all(), $relation);

        $data = [
            'data' => $roles,
            'title' => 'Manage Roles',
            'singleName' => 'role',
            'prefix' => 'roles',
            'delMultiRoute' => route('admin.ajax.destroy.multiple-roles'),
            'multiStatusRouteUpdate' => route('admin.ajax.update.multiple-roles-status'),
            'toggleStatusUpdate' => route('admin.ajax.update.role-status')
        ];

        return view('backend.pages.roles.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('module', 'roles.create');
        $data = $this->config();
        return view('backend.pages.roles.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create Or Update role',
            'parentBreadcrumb' => "Manage Roles",
            'prefix' => 'roles',
        ];
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoleRequest $request)
    {
        $this->authorize('module', 'roles.store');
        $this->roleRepository->create($request->all());

        return $this->redirectBack('msg', 'New item added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $this->authorize('module', 'roles.show');
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $this->authorize('module', 'roles.edit');
        $role = $this->roleRepository->findOrFail($id);
        $data = $this->config();
        $data['item'] = $role;
        return view('backend.pages.roles.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoleRequest $request, string $id)
    {
        $this->authorize('module', 'roles.update');

        $this->roleRepository->update($request->all(), $id);

        return $this->redirectBack('msg', 'Role updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->authorize('module', 'roles.destroy');

        $user = $this->checkHasUsers($id);

        if ($user) {
            return $this->redirectBack('err', 'Role exist for the user and cannot be deleted.');
        }

        $this->roleRepository->destroy($id);

        return $this->redirectBack('msg', 'Role deleted successfully.');
    }

    public function checkHasUsers($id)
    {
        return User::where('role_id', $id)->first('id');
    }
}
