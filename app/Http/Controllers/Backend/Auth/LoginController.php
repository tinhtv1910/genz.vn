<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('admin.dashboard');
        }
        return view('backend.auth.login');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(LoginRequest $request)
    {
        $fields = $request->only('email', 'password');
        $remember = (bool)$request->get('remember_me');

        if (Auth::attempt($fields, $remember)) {
            return redirect()->route('admin.dashboard');
        }
        
        return redirect()->route('admin.login.index')->with('err', 'Email or password is invalid.');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('admin.login.index')->with('succ', 'You logged out.');
    }
}
