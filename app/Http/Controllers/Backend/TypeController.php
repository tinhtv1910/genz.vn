<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\TypeRequest;
use App\Services\TypeService;
use Illuminate\Http\Request;

class TypeController extends Controller
{  
    public $typeService;

    public function __construct(TypeService $typeService)
    {
        $this->typeService = $typeService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data = $this->typeService->getList($request);

        return view('backend.pages.types.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = $this->typeService->createType();

        return view('backend.pages.types.createOrUpdate', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TypeRequest $request)
    {
        $this->typeService->storeType($request);
        return $this->redirectBack('msg', 'New type added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = $this->typeService->editType($id);

        return view('backend.pages.types.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TypeRequest $request, string $id)
    {
        $this->typeService->updateType($request, $id);

        return $this->redirectBack('msg', 'Type updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->typeService->deleteType($id);

        return $this->redirectBack('msg', 'Type deleted successfully.');
    }
}
