<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\PermissionRepository;
use App\Repositories\PermissionRoleRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class PermissionRoleController extends Controller
{
    public $roleRepository;
    public $permissionRepository;
    public $permissionRoleRepository;

    public function __construct(
        RoleRepository $roleRepository,
        PermissionRoleRepository $permissionRoleRepository,
        PermissionRepository $permissionRepository
    ) {
        $this->roleRepository = $roleRepository;
        $this->permissionRoleRepository = $permissionRoleRepository;
        $this->permissionRepository = $permissionRepository;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('module', 'permission-roles.index');

        $roles = $this->getListRole();
        $permissions = $this->getListPermission();
        $permissionRoles = $this->permissionRoleRepository->getAll(['limit' => 10000]);
        $dataPermissionRole = [];

        $data = [
            'roles' => $roles,
            'permissions' => $permissions,
            'title' => 'Assign Permission Role',
            'delMultiRoute' => '',
            'prefix' => 'permission-roles'
        ];

        foreach ($permissionRoles as $item) {
            $dataPermissionRole[$item->role_id][] = $item->permission_id;
        }
        
        if (count($dataPermissionRole)) {
            $data['dataPermissionRole'] = $dataPermissionRole;
        }

        return view('backend.pages.permission-role.index', $data);
    }

    public function getListRole()
    {
        return $this->roleRepository->getAll(['sorts' => 'id:asc']);
    }

    public function getListPermission()
    {
        return $this->permissionRepository->getAll(['limit' => 10000]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->authorize('module', 'permission-roles.index');
        
        if (!$request->get('permission')) {
            return $this->redirectBack('err', 'No permisson assigned to role');
        }

        $permissions = $request->get('permission');

        foreach ($permissions as $key => $permission) {
            $role = $this->roleRepository->findOrFail($key);
            $role->permissions()->sync($permission);
        }

        return $this->redirectBack('msg', 'Permission roles updated successfully.');
    }
}
