<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Sidebar;
use App\Repositories\SidebarRepository;
use Illuminate\Http\Request;

class SidebarController extends Controller
{
    public $sidebarRepository;

    public function __construct(SidebarRepository $sidebarRepository)
    {
        $this->sidebarRepository = $sidebarRepository;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $query = Sidebar::with(['sidebar']);
        $sidebars = $this->sidebarRepository->getAll($request->all(), $query);

        $data = [
            'data' => $sidebars,
            'title' => 'Manage Sidebar',
            'singleName' => 'sidebar',
            'prefix' => 'sidebars',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];

        return view('backend.pages.sidebars.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = $this->config();
        $data['sidebarOptions'] = $this->sidebarRepository->recursive();
        
        return view('backend.pages.sidebars.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create or update sidebar',
            'parentBreadcrumb' => "Manage sidebars",
            'prefix' => 'sidebars',
        ];
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if ($request['type_id'] == 0 && !$request['name']) return $this->redirectBack('err', 'Name is required.');
        if (!$request['slug']) return $this->redirectBack('err', 'Slug is required.');

        $this->sidebarRepository->create($request->all());

        return $this->redirectBack('msg', 'New sidebar added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $query = $this->sidebarRepository;
        $sidebar = $query->findOrFail($id);
        $data = $this->config();
        $data['sidebarOptions'] = $query->recursive($sidebar->parent_id);
        $data['item'] = $sidebar;
        
        return view('backend.pages.sidebars.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        if ($request['type_id'] == 0 && !$request['name']) return $this->redirectBack('err', 'Name is required.');
        if (!$request['slug']) return $this->redirectBack('err', 'Slug is required.');

        $this->sidebarRepository->update($request->all(), $id);

        return redirect()->route('admin.sidebars.index')->with('msg', 'Sidebar added successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->sidebarRepository->destroy($id);
        return $this->redirectBack('msg', 'Sidebar deleted successfully.');
    }
}
