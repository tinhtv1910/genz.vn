<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\LanguageRequest;
use App\Models\Language;
use App\Repositories\LanguageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LanguageController extends Controller
{
    public $languageRepository;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('module', 'languages.index');
        $languages = $this->languageRepository->getAll($request->all(), Language::with('user'));

        $data = [
            'data' => $languages,
            'title' => 'Manage Languages',
            'singleName' => 'language',
            'prefix' => 'languages',
            'delMultiRoute' => route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => route('admin.ajax.update.language-status')
        ];

        return view('backend.pages.languages.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('module', 'languages.create');
        $data = $this->config();
        return view('backend.pages.languages.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create Or Update language',
            'parentBreadcrumb' => "Manage Languages",
            'prefix' => 'languages',
        ];
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(LanguageRequest $request)
    {
        $this->authorize('module', 'languages.store');

        $request['user_id'] = Auth::id();

        $this->languageRepository->create($request->all());

        return $this->redirectBack('msg', 'New item added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $this->authorize('module', 'languages.show');
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $this->authorize('module', 'languages.edit');
        
        $item = $this->languageRepository->findOrFail($id);
        
        $data = $this->config();
        
        $data['item'] = $item;

        return view('backend.pages.languages.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(LanguageRequest $request, string $id)
    {
        $this->authorize('module', 'languages.update');
       
        $this->languageRepository->update($request->all(), $id);

        return $this->redirectBack('msg', 'Item updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->authorize('module', 'languages.destroy');

        $this->languageRepository->destroy($id);

        return $this->redirectBack('msg', 'Item deleted successfully.');
    }
}
