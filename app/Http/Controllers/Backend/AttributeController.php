<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\AttributeValue;
use App\Repositories\AttributeRepository;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public $attributeRepository;

    public function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    public function index(Request $request)
    {
        $attributes = $this->attributeRepository->getAll($request->all());
        $data = [
            'data' => $attributes,
            'title' => 'Manage Attributes',
            'singleName' => 'attribute',
            'prefix' => 'attributes',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];
        return view('backend.pages.attributes.index', $data);
    }

    public function show($id)
    {
    }
    public function edit($id)
    {
    }
    public function update(Request $request, $id)
    {
        $this->attributeRepository->update($request->all(), $id);
        return $this->successJson();
    }

    public function store(Request $request)
    {
        $request->validate([
            'short_name' => 'required',
            'name' => 'required',
        ]);

        $this->attributeRepository->create($request->all());

        return $this->redirectBack('msg', 'New attribute added successfully.');
    }

    public function storeValue(Request $request)
    {
        $request->validate([
            'attribute_id' => 'required',
        ]);

        $value = AttributeValue::where([
            'attribute_id' => $request['attribute_id'],
            'name' => $request['value']
        ])->first();

        if ($value) return $this->redirectBack('err', 'Already exists attribute with value.');

        $request['name'] = $request['value'];

        AttributeValue::create($request->all());

        return $this->redirectBack('msg', 'Value added successfully.');
    }

    public function updateValue(Request $request)
    {
        AttributeValue::findOrFail($request['id'])->update([
            'name' => $request['val']
        ]);

        return $this->successJson();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->attributeRepository->destroy($id);
        AttributeValue::where('attribute_id', $id)->delete();

        return $this->redirectBack('msg', 'Deleted attribute.');
    }

    public function getValueByAttributeId($id)
    {
        return $this->successJson(AttributeValue::where('attribute_id', $id)->get());
    }
}
