<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MenuRequest;
use App\Services\MenuService;
use App\Traits\UploadImage;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    use UploadImage;

    protected $menuService;

    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data = $this->menuService->listMenu($request);
        return view('backend.pages.menus.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = $this->menuService->createMenu();
        return view('backend.pages.menus.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create or Update menu',
            'parentBreadcrumb' => "Manage menu",
            'prefix' => 'menus',
        ];
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MenuRequest $request)
    {
        $data = $this->menuService->storeMenu($request);

        if (!$data) return $this->redirectBack('err', 'Exist slug.');
        
        return $this->redirectBack('msg', 'New menu added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = $this->menuService->editMenu($id);
        return view('backend.pages.menus.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MenuRequest $request, string $id)
    {
        $this->menuService->updateMenu($request, $id);
        return $this->redirectBack('msg', 'Menu updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->menuService->deleteMenu($id);
        return $this->redirectBack('msg', 'Menu deleted successfully.');
    }
}
