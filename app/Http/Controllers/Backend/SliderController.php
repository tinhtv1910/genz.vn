<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\SliderRepository;
use Illuminate\Http\Request;
use App\Traits\UploadImage;

class SliderController extends Controller
{
    use UploadImage;

    protected $sliderRepository;

    public function __construct(SliderRepository $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    public function index(Request $request)
    {
        $sliders = $this->sliderRepository->getAll($request->all());

        $data = [
            'data' => $sliders,
            'title' => 'Manage Sliders',
            'singleName' => 'slider',
            'prefix' => 'sliders',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-setting'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-settings-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.setting-status')
        ];
        return view('backend.pages.sliders.index', $data);
    }

    public function create()
    {
        $data = $this->config();
        return view('backend.pages.sliders.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create Or Update Sliders',
            'parentBreadcrumb' => "Manage Sliders",
            'prefix' => 'sliders',
        ];
    }

    public function edit($id) {
        $slider = $this->sliderRepository->findOrFail($id);

        $data = $this->config();
        $data['item'] = $slider;

        return view('backend.pages.sliders.createOrUpdate', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->has('file')) {
            $file = $request->file('file');
            $data['image'] = $this->uploadFile($file, 'banner', 'sliders');
        }

        $this->sliderRepository->create($data);

        return $this->redirectBack('msg', 'New slider added successfully.');
    }

    public function update(Request $request, $id) {
        $slider = $this->sliderRepository->findOrFail($id);
        $data = $request->all();

        if ($request->has('file')) {
            $file = $request->file('file');
            $data['image'] = $this->uploadFile($file, 'banner', 'sliders');
        }

        if ($slider->image) {
            $this->unlinkFile($slider->image);
        }

        $this->sliderRepository->update($data, $id);

        return $this->redirectBack('msg', 'New slider updated successfully.');
    }

    public function destroy($id) {
        $slider = $this->sliderRepository->findOrFail($id);

        if ($slider && $slider->image) {
            $this->unlinkFile($slider->image);
        } 

        $this->sliderRepository->destroy($id);

        return $this->redirectBack('msg', 'Slider deleted successfully.');
    }
}
