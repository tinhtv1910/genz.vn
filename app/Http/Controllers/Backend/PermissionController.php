<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('module', 'permissions.index');
        $permissions = $this->permissionRepository->getAll($request->all());

        $data = [
            'data' => $permissions,
            'title' => 'Manage Permissions',
            'singleName' => 'permission',
            'delMultiRoute' => route('admin.ajax.destroy.multiple-permissions'),
            'prefix' => 'permissions'
        ];

        return view('backend.pages.permissions.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('module', 'permissions.create');
        $data = $this->config();
        return view('backend.pages.permissions.createOrUpdate', $data);
    }
    
    public function config()
    {
        return [
            'title' => 'Create Or Update permission',
            'parentBreadcrumb' => "Manage Permissions",
            'prefix' => 'permissions',
        ];
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(PermissionRequest $request)
    {
        $this->authorize('module', 'permissions.store');
        $this->permissionRepository->create($request->all());

        return $this->redirectBack('msg', 'New language added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $this->authorize('module', 'permissions.show');
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $this->authorize('module', 'permissions.edit');
        $item = $this->permissionRepository->findOrFail($id);
        $data = $this->config();
        $data['item'] = $item;

        return view('backend.pages.permissions.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PermissionRequest $request, string $id)
    {
        $this->authorize('module', 'permissions.update');
        $this->permissionRepository->update($request->all(), $id);

        return $this->redirectBack('msg', 'Permission updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->authorize('module', 'permissions.destroy');
        $this->permissionRepository->destroy($id);

        return $this->redirectBack('msg', 'Item deleted successfully.');
    }
}
