<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index(Request $request)
    {
        $data = $this->productService->listProduct($request);
        return view('backend.pages.products.index', $data);
    }

    public function create()
    {
        $data = $this->productService->createProduct();
        return view('backend.pages.products.createOrUpdate', $data);
    }

    public function store(ProductRequest $request)
    {
        $this->productService->storeProduct($request);
        return $this->redirectBack('msg', 'New product added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    public function edit(string $id)
    {
        $data = $this->productService->editProduct($id);
        return view('backend.pages.products.createOrUpdate', $data);
    }

    public function update(ProductRequest $request, string $id)
    {
        $this->productService->updateProduct($request, $id);
        return $this->redirectBack('msg', 'Product updated successfully.');
    }

    public function destroy(string $id)
    {
        $this->productService->deleteProduct($id);
        return $this->redirectBack('msg', 'Product deleted successfully.');
    }
}
