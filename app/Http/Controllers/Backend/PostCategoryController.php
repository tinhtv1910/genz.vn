<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostCategoryRequest;
use App\Repositories\PostCategoryRepository;
use App\Traits\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostCategoryController extends Controller
{
    use UploadImage;

    public $postCategoryRepo;

    public function __construct(PostCategoryRepository $postCategoryRepo)
    {
        $this->postCategoryRepo = $postCategoryRepo;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $categories = $this->postCategoryRepo->getAll($request->all());

        $data = [
            'data' => $categories,
            'title' => 'Manage Post Categories',
            'singleName' => 'category',
            'prefix' => 'post-categories',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];

        return view('backend.pages.post-categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = $this->config();

        return view('backend.pages.post-categories.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create or Update category',
            'parentBreadcrumb' => "Manage post category",
            'prefix' => 'post-categories',
        ];
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(PostCategoryRequest $request)
    {
        $data = $request->all();

        if (!$request->get('slug')) {
            $data['slug'] = Str::slug($request->get('name'));
        }

        if ($request->has('meta_thumb')) {
            $file = $request->file('meta_thumb');
            $data['meta_thumb'] = $this->uploadFile($file, 'image', 'meta');
        }

        $this->postCategoryRepo->create($data);

        return $this->redirectBack('msg', 'New category added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $postCategory = $this->postCategoryRepo->findOrFail($id);
        $data = $this->config();
        $data['item'] = $postCategory;

        return view('backend.pages.post-categories.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PostCategoryRequest $request, string $id)
    {
        $postCategory = $this->postCategoryRepo->findOrFail($id);

        $data = $request->all();

        if (!$request->get('slug')) {
            $data['slug'] = Str::slug($request->get('name'));
        }

        if ($request->has('meta_thumb')) {
            $file = $request->file('meta_thumb');
            $data['meta_thumb'] = $this->uploadFile($file, 'image', 'meta');

            if ($postCategory->meta_thumb) {
                $this->unlinkFile($postCategory->meta_thumb);
            }
        }

        $postCategory->update($data);

        return $this->redirectBack('msg', 'New category updated successfully.');
    
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $postCategory = $this->postCategoryRepo->findOrFail($id);

        if (!$postCategory) return $this->redirectBack('err', 'Something went wrong.');
        
        if ($postCategory->meta_thumb) {
            $this->unlinkFile($postCategory->meta_thumb);
        } 
        
        $postCategory->delete();

        return $this->redirectBack('msg', 'Category deleted successfully.');
    }
}
