<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    protected $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    public function index(Request $request)
    {
        
        $typeSlugs = $this->settingRepository->getAll($request->all());

        $data = [
            'data' => $typeSlugs,
            'title' => 'Manage Settings',
            'singleName' => 'setting',
            'prefix' => 'settings',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-setting'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-settings-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.setting-status')
        ];

        return view('backend.pages.settings.index', $data);
    }

    public function create(Request $request)
    {
        $dataTypeSlugs = $this->settingRepository->getAll($request->all(), Setting::select('type_slug'));

        $typeSlugs = [];

        foreach ($dataTypeSlugs as $typeSlug) {
            $typeSlugs[] = $typeSlug->type_slug;
        }

        $data = $this->config();
        $data['typeSlugs'] = $typeSlugs;
        
        return view('backend.pages.settings.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create Or Update Settings',
            'parentBreadcrumb' => "Manage Settings",
            'prefix' => 'settings',
        ];
    }
}
