<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Repositories\PostCategoryRepository;
use App\Repositories\PostRepository;
use App\Traits\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PostController extends Controller
{
    use UploadImage;

    public $postRepository;
    public $postCategoryRepository;

    public function __construct(PostRepository $postRepository, PostCategoryRepository $postCategoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->postCategoryRepository = $postCategoryRepository;
    }

    public function index(Request $request)
    {
        $query = Post::with('postCategory', 'user');

        $posts = $this->postRepository->getAll($request->all(), $query);

        $data = [
            'data' => $posts,
            'title' => 'Manage Posts',
            'singleName' => 'post',
            'prefix' => 'posts',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];

        return view('backend.pages.posts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = $this->config();
        $categories = $this->postCategoryRepository->getAll();
        $data['categories'] = $categories;

        return view('backend.pages.posts.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create or Update post',
            'parentBreadcrumb' => "Manage posts",
            'prefix' => 'posts',
        ];
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $request)
    {
        $data = $request->all();

        if (!$request->get('slug')) {
            $data['slug'] = $this->convertStringToSlug($request->get('title'));
            $post = $this->getPostBySlug($data['slug']);

            if ($post) return redirect()->route('admin.posts.create')->with('err', 'Already exists post slug.');
        }

        if ($request->has('file')) {
            $data['image'] = $this->uploadFile($request->file('file'), 'thumb', 'posts');
        }

        $data['user_id'] = Auth::id();

        $this->postRepository->create($data);

        return $this->redirectBack('msg', 'New post added successfully.');
    }

    public function getPostBySlug($slug)
    {
        return $this->postRepository->findByField('slug', $slug);
    }

    public function convertStringToSlug($string)
    {
        return Str::slug($string);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = $this->config();

        $categories = $this->postCategoryRepository->getAll();

        $post = $this->postRepository->findOrFail($id);

        $data['categories'] = $categories;
        $data['item'] = $post;

        return view('backend.pages.posts.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PostRequest $request, string $id)
    {
        $data = $request->all();

        $post = $this->postRepository->find($id);

        if (!$post) return $this->redirectBack('err', 'Some thing went wrong.');

        if ($request->has('file')) {
            $data['image'] = $this->uploadFile($request->file('file'), 'thumb', 'posts');
            if ($post->image) {
                $this->unlinkFile($post->image);
            }
        }

        $post->update($data);

        return $this->redirectBack('msg', 'Post updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = $this->postRepository->findOrFail($id);

        if ($post->image) {
            $this->unlinkFile($post->image);
        }

        $post->delete();

        return $this->redirectBack('msg', 'Post deleted successfully.');
    }
}
