<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Traits\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use UploadImage;

    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $this->authorize('module', 'users.index');
        $relation = User::with(['province', 'district', 'ward', 'role']);
        $users = $this->userRepository->getAll($request->all(), $relation);
        $roles = $this->userRepository->getListRole();

        $data = [
            'data' => $users,
            'roles' => $roles,
            'title' => 'Manage Users',
            'singleName' => 'user',
            'delMultiRoute' => route('admin.ajax.destroy.multiple-user'),
            'prefix' => 'users',
            'multiStatusRouteUpdate' => route('admin.ajax.update.multiple-user-status'),
            'toggleStatusUpdate' => route('admin.ajax.update.user-status')
        ];

        return view('backend.pages.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('module', 'users.create');

        $provinces = $this->userRepository->getListProvince();

        $roles = $this->userRepository->getListRole();

        $data = $this->config();

        $data['roles'] = $roles;
        $data['provinces'] = $provinces;

        return view('backend.pages.users.createOrUpdate', $data);
    }

    public function config()
    {
        return [
            'title' => 'Create Or Update Users',
            'parentBreadcrumb' => "Manage Users",
            'prefix' => 'users',
        ];
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request)
    {
        $this->authorize('module', 'users.store');

        $data = $request->all();

        if ($request->has('file')) {
            $file = $request->file('file');
            $data['image'] = $this->uploadFile($file, 'thumb', 'users');
        }

        $this->userRepository->create($data);

        return $this->redirectBack('msg', 'New user added successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $this->authorize('module', 'users.show');
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $this->authorize('module', 'users.edit');

        $user = $this->userRepository->findOrFail($id);

        $provinces = $this->userRepository->getListProvince();

        $roles = $this->userRepository->getListRole();

        $data = $this->config();

        $data['roles'] = $roles;
        $data['provinces'] = $provinces;
        $data['user'] = $user;

        return view('backend.pages.users.createOrUpdate', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserRequest $request, string $id)
    {
        $this->authorize('module', 'users.update');
        $userData = $this->userRepository->findOrFail($id);
        $currentUser = Auth::user();

        if ($userData->role_id == 1 && $currentUser->role_id != $userData->role_id) {
            return $this->redirectBack('err', 'This action is Unauthorized.');
        }

        $data = $request->all();

        if ($request->has('file')) {
            $file = $request->file('file');
            $data['image'] = $this->uploadFile($file, 'thumb', 'users');
            if ($userData->image) {
                $this->unlinkFile($userData->image);
            }
        }

        $this->userRepository->update($data, $id);

        return $this->redirectBack('msg', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->authorize('module', 'users.destroy');

        $this->userRepository->destroy($id);

        return $this->redirectBack('msg', 'User deleted successfully.');
    }
}
