<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function successJson($data = [], $msg = "Success", $code = 200)
    {
        return response()->json([
            'code' => $code,
            'data' => $data,
            'msg' => $msg
        ]);
    }

    public function errorJson($data = [], $msg = "Error", $code = 400)
    {
        return response()->json([
            'code' => $code,
            'data' => $data,
            'msg' => $msg
        ]);
    }

    public function redirectBack($type = 'msg', $msg = 'Success')
    {
        return redirect()->back()->with($type, $msg);
    }
}
