<?php

namespace App\Models;

class Role extends BaseModel
{
    protected $fillable = [
        'name',
        'slug',
        'status'
    ];
    public $timestamps = true;

    public function filterSearch($query, $value)
    {
        return $query->where('name', 'LIKE', '%' . $value . '%');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, PermissionRole::class, 'role_id', 'permission_id')->withTimestamps();
        // role_id is foreignkey of role [current table]
        // permission_id foreignkey of related table
    }
}
