<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class PostCategory extends BaseModel
{
    protected $fillable = [
        'name',
        'slug',
        'order',
        'status',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'meta_thumb',
    ];

    public $timestamps = true;

    public function filterSearch($query, $value)
    {
        return $query->where('id', $value)->orWhere('name', 'LIKE', '%' . $value . '%');
    }

    public function getMetaImgAttribute()
    {
        return Storage::url($this->meta_thumb);
    }
}
