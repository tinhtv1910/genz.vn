<?php

namespace App\Models;

class PermissionRole extends BaseModel
{
    protected $fillable = [
        'role_id',
        'permission_id'
    ];

    public $timestamps = true;
}
