<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'attribute_value_id',
        'price',
        'quantity',
        'product_id'
    ];

    public $timestamps = true;
}
