<?php

namespace App\Models;

class Language extends BaseModel
{
    protected $table = "languages";

    protected $fillable = [
        'user_id',
        'name',
        'slug',
        'status'
    ];

    protected $filterable = ['status'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function filterSearch($query, $value)
    {
        return $query->where('name', 'LIKE', '%' . $value . '%')
            ->orWhere('slug', 'LIKE', '%' . $value . '%')
            ->orWhere('id', $value);
    }
}
