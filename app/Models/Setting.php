<?php

namespace App\Models;

class Setting extends BaseModel
{
    protected $guarded = ['id'];

    public $timestamps = true;

    public function filterSearch($query, $value)
    {
        return $query->where('title', 'LIKE', '%' . $value . '%');
    }
}
