<?php

namespace App\Models;

class Type extends BaseModel
{
    protected $fillable = [
        'name',
        'slug',
        'prefix'
    ];

    public $timestamps = true;
}
