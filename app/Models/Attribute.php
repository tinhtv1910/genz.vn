<?php

namespace App\Models;

class Attribute extends BaseModel
{
    protected $fillable = ['name', 'short_name'];

    public $timestamps = false;

    public function attributeValues()
    {
        return $this->hasMany(AttributeValue::class, 'attribute_id');
    }
}
