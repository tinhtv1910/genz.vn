<?php

namespace App\Models;

class Order extends BaseModel
{
    protected $fillable = ['status'];

    public function filterDate($query, $value) {
        $query->when($value, function ($q) use ($value) {
            return $q->whereDate('created_at', $value);
        });
    }

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class, 'status', 'id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }
}
