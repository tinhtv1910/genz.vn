<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Menu extends BaseModel
{
    protected $fillable = [
        'name',
        'slug',
        'order',
        'status',
        'parent_id',
        'type_id',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'meta_thumb',
        'image',
    ];

    public $timestamps = true;

    protected $filterable = ['id', 'parent_id'];

    public function type()
    {
        return $this->belongsTo(MenuType::class, 'type_id');
    }

    public function parentMenu()
    {
        return $this->belongsTo($this, 'parent_id');
    }

    public function getThumbAttribute()
    {
        return Storage::url($this->image);
    }

    public function getMetaImageAttribute()
    {
        return Storage::url($this->meta_thumb);
    }
}
