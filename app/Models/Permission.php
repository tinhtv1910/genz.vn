<?php

namespace App\Models;

class Permission extends BaseModel
{
    protected $fillable = [
        'name',
        'slug',
    ];

    public $timestamps = true;
}
