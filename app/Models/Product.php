<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Product extends BaseModel
{
    protected $guarded = ['id'];

    public $timestamps = true;

    public function menus()
    {
        return $this->belongsToMany(Menu::class, ProductCategory::class, 'product_id', 'menu_id')->withTimestamps();
    }

    public function getThumbAttribute()
    {
        return Storage::url($this->image);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function productImages()
    {
        return $this->hasMany(ProductImage::class, 'product_id');
    }

    public function attributeValues()
    {
        return $this->belongsToMany(AttributeValue::class, ProductAttribute::class, 'product_id', 'attribute_value_id')->withTimestamps();
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }
}
