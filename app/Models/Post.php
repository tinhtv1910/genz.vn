<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Post extends BaseModel
{
    protected $table = 'posts';

    protected $fillable = [
        'title',
        'slug',
        'post_category_id',
        'image',
        'description',
        'content',
        'user_id',
        'status',
        'keywords',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    public $timestamps = true;

    public function getThumbAttribute()
    {
        return Storage::url($this->image);
    }

    public function postCategory()
    {
        return $this->belongsTo(PostCategory::class, 'post_category_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
