<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Slider extends BaseModel
{
    protected $table = 'sliders';

    protected $fillable = [
        'title',
        'link',
        'description',
        'image',
        'status',
        'order'
    ];

    public function getThumbAttribute()
    {
        return Storage::url($this->image);
    }
}
