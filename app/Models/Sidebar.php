<?php

namespace App\Models;

class Sidebar extends BaseModel
{
    protected $fillable = [
        'name',
        'slug',
        'type_id',
        'icon',
        'order',
        'parent_id'
    ];

    public $timestamps = true;

    public function sidebar()
    {
        return $this->belongsTo($this, 'parent_id');
    }

    public function subMenus()
    {
        return $this->hasMany($this, 'parent_id');
    }
}
