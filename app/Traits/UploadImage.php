<?php

namespace App\Traits;

use Intervention\Image\Image;
use Intervention\Image\ImageManagerStatic;

trait UploadImage
{
    public function uploadFile($file, $type, $category = '', $index = 1): string
    {
        $path = 'S:/CODE/MY_PROJECT/BIG PROJECT/genz-cdn/storage/app/public/' . $category;

        $fileExtension = $file->extension();

        $image_resize = $this->resizeImg($file, $type);

        $fileHash = 'genzvn_' . date('YmdHims') . $index . '_' . $category . '_' . time() . '.' . $fileExtension;

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $fileSave = $path . '/' . $fileHash;
        $image_resize->save($fileSave);

        return $category . '/' . $fileHash;
    }

    public function unlinkFile($filePath): void
    {
        $url = 'S:/CODE/MY_PROJECT/BIG PROJECT/genz-cdn/storage/app/public/' . $filePath;
        if (file_exists($url)) {
            unlink($url);
        }
    }

    protected function resizeImg($file, $type): Image
    {
        $image_resize = ImageManagerStatic::make($file);
        list($width) = getimagesize($file);

        if ($type == 'thumb') {
            if ($width > 200) {
                $image_resize->resize(200, null, function ($const) {
                    $const->aspectRatio();
                    $const->upsize();
                });
            }
        }

        if ($type == 'image') {
            if ($width > 800) {
                $image_resize->resize(800, null, function ($const) {
                    $const->aspectRatio();
                    $const->upsize();
                });
            }
        }

        if ($type == 'banner') {
            if ($width > 2000) {
                $image_resize->resize(2000, null, function ($const) {
                    $const->aspectRatio();
                    $const->upsize();
                });
            }
        }

        return $image_resize;
    }
}
