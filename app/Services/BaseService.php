<?php

namespace App\Services;

use App\Repositories\BaseRepository;

/**
 * Class BaseService
 * @package App\Services
 */
class BaseService
{
    protected $baseRepository;

    public function __construct(BaseRepository $baseRepository)
    {
        $this->baseRepository = $baseRepository;
    }

    public function logic()
    {
        return 'Here!';
    }
}
