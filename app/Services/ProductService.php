<?php

namespace App\Services;

use App\Models\Attribute;
use App\Models\Menu;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Type;
use App\Repositories\ProductRepository;
use App\Traits\UploadImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductService
{
    use UploadImage;

    public $productRepository;
    public $productImage;

    public function __construct(ProductRepository $productRepository, ProductImage $productImage)
    {
        $this->productRepository = $productRepository;
        $this->productImage = $productImage;
    }

    public function listProduct($request)
    {
        $query = Product::with('user');
        $products = $this->productRepository->getAll($request->all(), $query);
        $data = [
            'data' => $products,
            'title' => 'Manage Products',
            'singleName' => 'product',
            'prefix' => 'products',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];

        return $data;
    }

    public function config()
    {
        return [
            'title' => 'Create or update product',
            'parentBreadcrumb' => "Manage products",
            'prefix' => 'products',
        ];
    }

    public function listCategories()
    {
        $categories = Menu::orderBy('name', 'asc')->get();
        return $categories;
    }

    public function getListAttribute()
    {
        return Attribute::all();
    }

    public function getListType()
    {
        return Type::all();
    }

    public function createProduct()
    {
        $data = $this->config();
        $data['categories'] = $this->listCategories();
        $data['attributes'] = $this->getListAttribute();
        $data['types'] = $this->getListType();

        return $data;
    }

    public function storeProduct($request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();

        if ($request->hasFile('file')) {
            $data['image'] = $this->uploadFile($request->file('file'), 'thumb', 'products');
        }

        $product = $this->productRepository->create($data);

        $this->storeProductAttribute($product, $request);

        if ($product && $request->hasFile('productImgs')) {
            $index = 1;
            foreach ($request->file('productImgs') as $file) {
                $image = $this->uploadFile($file, 'image', 'products', $index);
                $this->storeProductImage($product->id, $image);
                $index++;
            }
        }

        if ($request['categories']) {
            $categories = $request['categories'];
            $product->menus()->attach($categories);
        }
    }

    public function storeProductAttribute($product, $request)
    {
        $data = [];
        if ($request['attributeValues']) {
            $attributeValues = $request['attributeValues'];
            $productAttributePrices = $request['productAttributePrices'];
            $productAttributeQuantities = $request['productAttributeQuantities'];

            foreach ($attributeValues as $value) {
                $data[] = [
                    'attribute_value_id' => $value,
                    'price' => $productAttributePrices[$value],
                    'quantity' => $productAttributeQuantities[$value],
                ];
            }
        }

        $product->attributeValues()->sync($data);
    }

    public function storeProductImage($productId, $image)
    {
        $this->productImage->create([
            'product_id' => $productId,
            'image' => $image
        ]);
    }

    public function editProduct($id)
    {
        $product = $this->productRepository->findOrFail($id);
        $data = $this->config();
        $data['categories'] = $this->listCategories();
        $data['item'] = $product;
        $data['types'] = $this->getListType();

        if (count($product->productAttributes)) {
            foreach ($product->productAttributes as $item) {
                $data['attributeValues'][] = $item->attribute_value_id;
                $data['productAttributePrices'][$item->attribute_value_id] = $item->price;
                $data['productAttributeQuantities'][$item->attribute_value_id] = $item->quantity;
            }
        }

        $cateIds = [];

        if (count($product->menus)) {
            foreach ($product->menus as $menu) {
                $cateIds[] = $menu->id;
            }
        }

        $data['cateIds'] = $cateIds;

        foreach ($product->productImages as $imageItem) {
            $data['images'][] = [
                'image' => Storage::url($imageItem->image),
                'id' => $imageItem->id
            ];
        }
        $data['attributes'] = $this->getListAttribute();

        return $data;
    }

    public function updateProduct($request, $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $data = $request->all();
        $data['user_id'] = Auth::id();

        if ($request->hasFile('file')) {
            $data['image'] = $this->uploadFile($request->file('file'), 'thumb', 'products');
            if ($product->image) {
                $this->unlinkFile($product->image);
            }
        }

        if ($request->hasFile('productImgs')) {
            $index = 1;
            foreach ($request->file('productImgs') as $file) {
                $image = $this->uploadFile($file, 'image', 'products', $index);
                $this->storeProductImage($product->id, $image);
                $index++;
            }
        }

        $product->update($data);
        $this->storeProductAttribute($product, $request);

        if ($request['categories']) {
            $categories = $request['categories'];
            $product->menus()->sync($categories);
        }
    }

    public function deleteProduct($id)
    {
        $product = $this->productRepository->findOrFail($id);

        if ($product->image) {
            $this->unlinkFile($product->image);
        }

        $productImageQuery = $this->productImage->where('product_id', $product->id);
        $productImages = $productImageQuery->get();
        foreach ($productImages as $porductImg) {
            if ($porductImg->image) {
                $this->unlinkFile($porductImg->image);
            }
        }
        $productImageQuery->delete();

        $product->menus()->detach();
        $product->attributeValues()->detach();

        $product->delete();
    }
}
