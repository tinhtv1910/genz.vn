<?php

namespace App\Services;

use App\Repositories\TypeRepository;

/**
 * Class TypeService
 * @package App\Services
 */
class TypeService
{
    public $typeRepository;

    public function __construct(TypeRepository $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    public function getList($request)
    {
        $types = $this->typeRepository->getAll($request->all());
        $data = [
            'data' => $types,
            'title' => 'Manage Types',
            'singleName' => 'type',
            'prefix' => 'types',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];

        return $data;
    }

    public function createType()
    {
        $data = $this->config();

        return $data;
    }

    public function config()
    {
        return [
            'title' => 'Create or update type',
            'parentBreadcrumb' => "Manage types",
            'prefix' => 'types',
        ];
    }

    public function storeType($request)
    {
        $this->typeRepository->create($request->all());
    }

    public function editType($id)
    {
        $data = $this->config();

        $data['item'] = $this->typeRepository->findOrFail($id);

        return $data;
    }

    public function updateType($request, $id)
    {
        $this->typeRepository->update($request->all(), $id);
    }

    public function deleteType($id)
    {
        $this->typeRepository->destroy($id);
    }
}
