<?php

namespace App\Services;

use App\Models\Menu;
use App\Repositories\MenuRepository;
use App\Traits\UploadImage;
use Illuminate\Support\Str;

/**
 * Class MenuService
 * @package App\Services
 */
class MenuService
{
    use UploadImage;
    protected $menuRepository;

    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    public function listMenu($request)
    {
        $relations = Menu::with(['type', 'parentMenu']);

        $menus = $this->menuRepository->getAll($request->all(), $relations);

        $data = [
            'data' => $menus,
            'title' => 'Manage Menu',
            'singleName' => 'menu',
            'prefix' => 'menus',
            'delMultiRoute' => '', //route('admin.ajax.destroy.multiple-language'),
            'multiStatusRouteUpdate' => '', //route('admin.ajax.update.multiple-language-status'),
            'toggleStatusUpdate' => '', //route('admin.ajax.update.language-status')
        ];

        return $data;
    }

    public function createMenu()
    {
        $data = $this->config();

        $data['menuOptions'] = $this->menuRepository->recursive();
        $data['types'] = $this->menuRepository->getMenuType();

        return $data;
    }

    public function config()
    {
        return [
            'title' => 'Create or Update menu',
            'parentBreadcrumb' => "Manage menu",
            'prefix' => 'menus',
        ];
    }

    public function storeMenu($request)
    {
        $data = $request->all();

        if (!$request->slug) {
            $data['slug'] = Str::slug($request->name);
        }

        $menu = $this->menuRepository->findByField('slug', $data['slug']);

        if ($menu) return false;

        if ($request->has('meta_thumb')) {
            $file = $request->file('meta_thumb');
            $data['meta_thumb'] = $this->uploadFile($file, 'image', 'meta');
        }

        if ($request->has('file')) {
            $file = $request->file('file');
            $data['image'] = $this->uploadFile($file, 'thumb', 'menus');
        }

        return $this->menuRepository->create($data);
    }

    public function editMenu($id)
    {
        $menu = $this->menuRepository->findOrFail($id);

        $data = $this->config();

        $data['menuOptions'] = $this->menuRepository->recursive($menu->parent_id);
        $data['types'] = $this->menuRepository->getMenuType();
        $data['item'] = $menu;

        return $data;
    }

    public function updateMenu($request, $id) 
    {
        $data = $request->all();

        if (!$request->slug) {
            $data['slug'] = Str::slug($request->name);
        }
        
        $menu = $this->menuRepository->findOrFail($id);

        if ($request->has('meta_thumb')) {
            $file = $request->file('meta_thumb');
            $data['meta_thumb'] = $this->uploadFile($file, 'image', 'meta');

            if ($menu->meta_thumb) {
                $this->unlinkFile($menu->meta_thumb);
            }
        }

        if ($request->has('file')) {
            $file = $request->file('file');
            $data['image'] = $this->uploadFile($file, 'thumb', 'menus');

            if ($menu->image) {
                $this->unlinkFile($menu->image);
            }
        }

        $menu->update($data);
    }

    public function deleteMenu($id)
    {
        $menu = $this->menuRepository->findOrFail($id);

        if ($menu->image) {
            $this->unlinkFile($menu->image);
        }

        $menu->delete();
    }
}
