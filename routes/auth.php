<?php

use App\Http\Controllers\Ajax\AjaxLanguageController;
use App\Http\Controllers\Ajax\AjaxPermissionController;
use App\Http\Controllers\Ajax\AjaxProductImageController;
use App\Http\Controllers\Ajax\AjaxRoleController;
use App\Http\Controllers\Ajax\AjaxUserController;
use App\Http\Controllers\Backend\AttributeController;
use App\Http\Controllers\Backend\Auth\LoginController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\LanguageController;
use App\Http\Controllers\Backend\MenuController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Backend\PermissionController;
use App\Http\Controllers\Backend\PermissionRoleController;
use App\Http\Controllers\Backend\PostCategoryController;
use App\Http\Controllers\Backend\PostController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\SidebarController;
use App\Http\Controllers\Backend\SliderController;
use App\Http\Controllers\Backend\TypeController;
use App\Http\Controllers\Backend\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', [LoginController::class, 'index'])->name('login.index');
Route::post('login', [LoginController::class, 'store'])->name('login.store');

Route::group(['middleware' => ['user.check', 'is.admin']], function () {

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::post('logout', [LoginController::class, 'logout'])->name('logout');

    Route::resource('users', UserController::class);
    Route::post('ajax/update/user-status', [AjaxUserController::class, 'updateUserStatus'])->name('ajax.update.user-status');
    Route::post('ajax/destroy/multiple-user', [AjaxUserController::class, 'destroyMultiple'])->name('ajax.destroy.multiple-user');
    Route::post('ajax/update/multiple-user-status', [AjaxUserController::class, 'multiUserStatusUpdate'])->name('ajax.update.multiple-user-status');

    Route::resource('languages', LanguageController::class);
    Route::post('ajax/update/language-status', [AjaxLanguageController::class, 'updateLanguageStatus'])->name('ajax.update.language-status');
    Route::post('ajax/destroy/multiple-language', [AjaxLanguageController::class, 'destroyMultiple'])->name('ajax.destroy.multiple-language');
    Route::post('ajax/update/multiple-language-status', [AjaxLanguageController::class, 'multiLanguageStatusUpdate'])->name('ajax.update.multiple-language-status');

    Route::resource('posts', PostController::class);

    Route::resource('post-categories', PostCategoryController::class);

    Route::resource('permissions', PermissionController::class);
    Route::post('ajax/destroy/multiple-permissions', [AjaxPermissionController::class, 'destroyMultiple'])->name('ajax.destroy.multiple-permissions');

    Route::resource('roles', RoleController::class);
    Route::post('ajax/update/role-status', [AjaxRoleController::class, 'updateRoleStatus'])->name('ajax.update.role-status');
    Route::post('ajax/destroy/multiple-roles', [AjaxRoleController::class, 'destroyMultiple'])->name('ajax.destroy.multiple-roles');
    Route::post('ajax/update/multiple-roles-status', [AjaxRoleController::class, 'multiRoleStatusUpdate'])->name('ajax.update.multiple-roles-status');

    Route::resource('permission-roles', PermissionRoleController::class);

    Route::resource('settings', SettingController::class);

    Route::resource('sliders', SliderController::class);

    Route::resource('menus', MenuController::class);

    Route::resource('sidebars', SidebarController::class);

    Route::resource('attributes', AttributeController::class);
    Route::get('attributes/{id}/value/', [AttributeController::class, 'getValueByAttributeId'])->name('attributes.value.attr_id');
    Route::post('attributes/value', [AttributeController::class, 'storeValue'])->name('attributes.value');
    Route::post('attributes/value/update', [AttributeController::class, 'updateValue'])->name('attributes.value.update');

    Route::resource('products', ProductController::class);

    Route::resource('types', TypeController::class);

    Route::post('product-image/destroy/{id}', [AjaxProductImageController::class, 'destroy'])->name('product-image.destroy');

    Route::resource('orders', OrderController::class);
    Route::get('orders/invoice/{id}', [OrderController::class, 'showInvoice'])->name('orders.invoice');
    Route::get('orders/invoice/{id}/download', [OrderController::class, 'downloadInvoice'])->name('orders.invoice.download');
});
