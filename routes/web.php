<?php

use App\Http\Controllers\Ajax\AjaxAdressController;
use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('ajax/address', [AjaxAdressController::class, 'getAdressById'])->name('ajax.address');

Route::any('image/upload', [UploadController::class, 'upload'])->name('image.upload');