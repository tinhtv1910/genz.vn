<table class="border-collapse w-full text-sm">
    <thead>
        <th class="border bg-gray-50 uppercase p-2 text-center">
            <input type="checkbox" id="mainCheckbox">
        </th>
        <th class="border bg-gray-50 uppercase p-2 text-center">#</th>
        <th class="border bg-gray-50 uppercase p-2">Image</th>
        <th class="border bg-gray-50 uppercase p-2">Name</th>
        <th class="border bg-gray-50 uppercase p-2">Slug</th>
        <th class="border bg-gray-50 uppercase p-2">Order</th>
        <th class="border bg-gray-50 uppercase p-2">Parent</th>
        <th class="border bg-gray-50 uppercase p-2">Type</th>
        <th class="border bg-gray-50 uppercase p-2">Prefix</th>
        <th class="border bg-gray-50 uppercase p-2">Status</th>
        <th class="border bg-gray-50 uppercase p-2"></th>
    </thead>
    <tbody>
        @forelse ($data as $item)
            <tr>
                <td class="border p-2 text-center">
                    <input type="checkbox" id="checkboxItem" value="{{ $item->id }}">
                </td>
                <td class="border p-2 text-center">
                    {{ $item->id }}
                </td>
                <td class="border p-2 text-center">
                    <div class="w-12 h-12 mx-auto flex items-center justify-center p-0.5 bg-gradient-to-br from-blue-700 to-white rounded-full border">
                        @if ($item->image)
                            <img src="{{ $item->thumb }}" class="rounded-full w-full h-full" alt="">
                        @else
                            -
                        @endif
                    </div>
                </td>
                <td class="border p-2">
                    {{ $item->name }}
                </td>
                <td class="border p-2">
                    {{ $item->slug }}
                </td>
                <td class="border p-2">
                    <input type="number" name="" max="100" step="5" min="0"
                        class="orderNumber outline-none border w-full p-2 rounded-sm" value="{{ $item->order ?? 0 }}"
                        data-id="{{ $item->id }}">
                </td>
                <td class="border p-2">
                    @if ($item->parent_id > 0)
                        <a href="?id={{ $item->parent_id }}" class="text-blue-500 hover:underline">
                            {{ $item->parent_id }} - {{ $item->parentMenu->name ?? '' }}
                        </a>
                    @else
                        {{ $item->parent_id }}
                    @endif
                </td>
                <td class="border p-2 text-center">
                    {{ $item->type->name ?? '' }}
                </td>
                <td class="border p-2 text-center">
                    {{ $item->sku_prefix ?? '-'}}
                </td>
                <td class="border p-2">
                    <div class="flex flex-col items-center gap-2">
                        <label class="cursor-pointer sw-toggle">
                            <input id="toggleStatusCheckbox{{ $item->id }}" data-id="{{ $item->id }}"
                                type="checkbox" class="toggle-checkbox" data-url="{{ $toggleStatusUpdate }}"
                                value="{{ $item->status }}" {{ $item->status ? 'checked' : '' }} />
                        </label>
                    </div>
                </td>
                <td class="border p-2">
                    <div class="flex flex-wrap justify-center gap-1">
                        <a href="{{ route('admin.' . $prefix . '.edit', $item->id) }}"
                            class="bg-blue-700 rounded-sm w-8 h-7 flex items-center justify-center text-white">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                                class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                <path
                                    d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.5.5 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11z" />
                            </svg>
                        </a>
                        <a href="#"
                            onclick="event.preventDefault(); return confirm('Are you sure to delete item {{ $item->name }}?') ? document.getElementById('deleteForm{{ $item->id }}').submit() : false"
                            class="bg-red-500 rounded-sm w-8 h-7 flex items-center justify-center text-white">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                                class="bi bi-trash2-fill" viewBox="0 0 16 16">
                                <path
                                    d="M2.037 3.225A.7.7 0 0 1 2 3c0-1.105 2.686-2 6-2s6 .895 6 2a.7.7 0 0 1-.037.225l-1.684 10.104A2 2 0 0 1 10.305 15H5.694a2 2 0 0 1-1.973-1.671zm9.89-.69C10.966 2.214 9.578 2 8 2c-1.58 0-2.968.215-3.926.534-.477.16-.795.327-.975.466.18.14.498.307.975.466C5.032 3.786 6.42 4 8 4s2.967-.215 3.926-.534c.477-.16.795-.327.975-.466-.18-.14-.498-.307-.975-.466z" />
                            </svg>
                        </a>

                        <button
                            class="bg-blue-400 rounded-sm w-8 h-7 flex items-center justify-center text-white">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0"/>
                                <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8m8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7"/>
                              </svg>
                        </button>

                        <form id="deleteForm{{ $item->id }}" class="hidden"
                            action="{{ route('admin.' . $prefix . '.destroy', $item->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="20" class="p-3 border">[Empty item]</td>
            </tr>
        @endforelse
    </tbody>
    @if ($data->count())
        <tfoot>
            <tr>
                <td colspan="20" class="border p-2">
                    <div>{{ $data->links('pagination::tailwind') }}</div>
                </td>
            </tr>
        </tfoot>
    @endif
</table>
