@extends('backend.layouts.page')

@section('title', $title)

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <a href="{{ route('admin.' . $prefix . '.index') }}" class="hover:text-blue-500">
                {{ $parentBreadcrumb }}
            </a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        {{-- NOTIFICATION --}}
        @include('backend.pages.components.noti')

        <form
            action="{{ isset($item) ? route('admin.' . $prefix . '.update', $item->id) : route('admin.' . $prefix . '.store') }}"
            method="POST" enctype="multipart/form-data">
            @csrf
            @if (isset($item))
                @method('PUT')
                <input type="hidden" name="id" class="hidden" value="{{ $item->id }}">
            @else
                @method('POST')
            @endif
            <div class="flex gap-5">
                <fieldset class="w-full px-5 py-2 border bg-gray-50 rounded-sm">
                    <legend class="text-xs font-medium text-gray-500">
                        Infomation
                    </legend>
                    <div class="flex flex-wrap gap-3">
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Title
                            </div>
                            <input type="text" placeholder="Enter title" name="title"
                                value="{{ old('title', $item->title ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('title')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Link
                            </div>
                            <input type="text" placeholder="Enter link" name="link"
                                value="{{ old('link', $item->link ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('link')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-full">
                            <div class="font-medium mb-1">
                                Description
                            </div>
                            <textarea name="description" rows="5" class="w-full p-2 outline-none rounded-sm border focus:border-gray-800"
                                placeholder="Enter slider description">{{ old('link', $item->link ?? '') }}</textarea>
                            @error('description')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-full">
                            <div class="font-medium mb-1">
                                Order
                            </div>
                            <input type="number" placeholder="Order setting" name="order"
                                value="{{ old('order', $item->order ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('order')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        
                        <div class="text-sm w-[calc(100%/2-10px)] flex flex-col gap-3">
                            <label class="w-full flex flex-col gap-1">
                                <span class="font-medium mb-1">Image</span>
                                <input id="changeThumbAvatar" type="file" name="file" class="">
                            </label>
                            <div class="">
                                <img src="" alt="" class="img-thumbnail max-h-[200px]" id="image-preview"
                                    data-ijabo-default-img="{{ isset($item) ? $item->thumb : '' }}">
                            </div>
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Active:
                            </div>

                            <div class="flex gap-3">
                                <label class="cursor-pointer border rounded-sm px-4 py-1">
                                    <input type="radio" class="status" id="block-button" hidden value="0"
                                        {{ isset($item) && $item->status == 0 ? 'checked' : '' }} name="status">
                                    <span>OFF</span>
                                </label>

                                <label class="cursor-pointer border rounded-sm px-4 py-1">
                                    <input type="radio" class="status" id="active-button" hidden value="1"
                                        {{ (isset($item) && $item->status == 1) || !isset($item) ? 'checked' : '' }}
                                        name="status">
                                    <span>ON</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="mt-5 flex items-center gap-3 justify-end">
                <a href="{{ route('admin.' . $prefix . '.index') }}"
                    class="py-2 px-4 bg-gray-200 rounded-sm hover:opacity-75" type="button">Cancel</a>
                <button class="py-2 px-4 bg-gray-800 text-yellow-400 rounded-sm hover:opacity-75">
                    {{ isset($item) ? 'Update' : 'Create' }}
                </button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script src="{{ asset('assets/be/js/create/status.js') }}"></script>
    <script src="{{ asset('vendor/ijabo-image/viewer.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/ijaboViewer.js') }}"></script>
@endpush
