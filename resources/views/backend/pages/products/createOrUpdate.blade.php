@extends('backend.layouts.page')

@section('title', $title)

@push('style')
    {{-- <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}"> --}}
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    {{-- <style>
        .select2-container--default .select2-selection--multiple {
            height: 40px;
            border-radius: 2px;
            border: 1px solid #d7d7d7;
        }
    </style> --}}
@endpush

@section('content')

    {{-- BREADCRUMB --}}
    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <a href="{{ route('admin.' . $prefix . '.index') }}" class="hover:text-blue-500">
                {{ $parentBreadcrumb }}
            </a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        {{-- NOTIFICATION --}}
        @include('backend.pages.components.noti')

        <form
            action="{{ isset($item) ? route('admin.' . $prefix . '.update', $item->id) : route('admin.' . $prefix . '.store') }}"
            method="POST" enctype="multipart/form-data">
            @csrf
            @if (isset($item))
                @method('PUT')
                <input type="hidden" name="id" class="hidden" value="{{ $item->id }}">
            @else
                @method('POST')
            @endif
            <div class="flex gap-5">
                <fieldset class="w-full px-5 py-2 border bg-gray-50 rounded-sm">
                    <legend class="text-xs font-medium text-gray-500">
                        Infomation
                    </legend>
                    <div class="flex flex-wrap gap-3">
                        {{-- NAME --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Name
                            </div>
                            <input type="text" placeholder="Enter name" name="name"
                                value="{{ old('name', $item->name ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('name')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- SLUG --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Slug
                            </div>
                            <input type="text" placeholder="Enter slug" name="slug"
                                value="{{ old('slug', $item->slug ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('slug')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- PRICE --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Price
                            </div>
                            <input type="text" placeholder="Enter price" name="price"
                                value="{{ old('price', $item->price ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('price')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- QUANTITY --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Quantity
                            </div>
                            <input type="text" placeholder="Enter quantity" name="quantity"
                                value="{{ old('quantity', $item->quantity ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('quantity')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- DESCRIPTION --}}
                        <div class="text-sm w-full">
                            <div class="font-medium mb-1">
                                Description
                            </div>
                            <textarea name="description" class="ck-editor">{{ old('description', $item->description ?? '') }}</textarea>
                            @error('description')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- COTENT --}}
                        <div class="text-sm w-full">
                            <div class="font-medium mb-1">
                                Content
                            </div>
                            <textarea name="content" class="ck-editor">{{ old('content', $item->content ?? '') }}</textarea>
                            @error('content')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- CATEGORIES --}}
                        <div class="mt-5 w-full">
                            <div class="flex items-center gap-5">
                                <div class="text-sm font-medium mb-1">Select categories</div>
                                <label
                                    class="p-1 text-sm flex items-center gap-2 cursor-pointer rounded-sm bg-white border">
                                    <input type="checkbox" class="selectAllCategories">
                                    <span>Select all</span>
                                </label>
                            </div>
                            <div class="flex text-sm flex-wrap">
                                @forelse($categories as $key => $category)
                                    <div class="w-1/6 p-1">
                                        <label
                                            class="w-full bg-white p-2 flex items-center gap-3 rounded-sm border cursor-pointer hover:border-blue-500">
                                            <input type="checkbox" value="{{ $category->id }}"
                                                {{ in_array($category->id, old('categories') ?? []) || (isset($cateIds) && in_array($category->id, $cateIds)) ? 'checked' : '' }}
                                                name="categories[]" class="categoryBox">
                                            <span>{{ $category->name }}</span>
                                        </label>
                                    </div>
                                @empty
                                    -
                                @endforelse
                            </div>
                        </div>

                        {{-- SKU PRODUCT --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Mã sản phẩm
                            </div>

                            <div class="flex flex-wrap gap-1">
                                @foreach ($types as $type)
                                    <label class="w-[calc(100%/3-3px)] p-2 rounded-sm bg-white border cursor-pointer">
                                        <input type="radio" value="{{ $type->id }}" name="type_id"
                                            data-prefix="{{ $type->prefix }}"
                                            {{ old('type_id', $item->type_id ?? '') == $type->id ? 'checked' : '' }}>
                                        <span class="">{{ $type->name }}</span>
                                    </label>
                                @endforeach
                            </div>
                            @error('type_id')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- PRODUCT ATTRIBUTE --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Select attribute
                            </div>
                            <select name="attribute_id" id="attribute"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                <option value="">--Select attribute--</option>
                                @foreach ($attributes as $attribute)
                                    <option value="{{ $attribute->id }}"
                                        {{ old('attribute_id', $item->attribute_id ?? '') == $attribute->id ? 'selected' : '' }}>
                                        {{ $attribute->name }}
                                    </option>
                                @endforeach
                            </select>

                            <div id="attributeValue" class="mt-1 flex flex-col gap-3"></div>
                        </div>

                        {{-- SKU PRODUCT --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                SKU
                            </div>
                            <input type="text" placeholder="Enter sku" name="sku"
                                value="{{ old('sku', $item->sku ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('sku')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        {{-- STATUS --}}
                        <div class="flex w-full justify-between gap-3 items-center">
                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Featured:
                                </div>

                                <div class="flex gap-3">
                                    <label class="cursor-pointer border rounded-sm px-4 py-1">
                                        <input type="radio" class="status" id="block-button" hidden value="0"
                                            {{ (isset($item) && $item->is_featured == 0) || !isset($item) ? 'checked' : '' }}
                                            name="is_featured">
                                        <span>OFF</span>
                                    </label>

                                    <label class="cursor-pointer border rounded-sm px-4 py-1">
                                        <input type="radio" class="status" id="active-button" hidden value="1"
                                            {{ isset($item) && $item->is_featured == 1 ? 'checked' : '' }}
                                            name="is_featured">
                                        <span>ON</span>
                                    </label>
                                </div>
                            </div>

                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Active:
                                </div>

                                <div class="flex gap-3">
                                    <label class="cursor-pointer border rounded-sm px-4 py-1">
                                        <input type="radio" class="status" id="block-button" hidden value="0"
                                            {{ isset($item) && $item->status == 0 ? 'checked' : '' }} name="status">
                                        <span>OFF</span>
                                    </label>

                                    <label class="cursor-pointer border rounded-sm px-4 py-1">
                                        <input type="radio" class="status" id="active-button" hidden value="1"
                                            {{ (isset($item) && $item->status == 1) || !isset($item) ? 'checked' : '' }}
                                            name="status">
                                        <span>ON</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        {{-- IMAGES --}}
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <label class="w-full flex flex-col gap-1 cursor-pointer">
                                <span class="font-medium mb-1">
                                    Select product images
                                </span>
                                <input id="multipleProduct" type="file" multiple name="productImgs[]"
                                    class="w-full border rounded-sm px-2 py-1 bg-white">
                            </label>
                            <div class="mt-3 flex flex-wrap gap-1">
                                <div id="upload__img-wrap" class="flex flex-wrap gap-1">
                                </div>

                                <div class="flex flex-wrap gap-1">
                                    @if (isset($images))
                                        @foreach ($images as $image)
                                            <div class="w-24 h-24 rounded-sm relative border border-blue-500 group">
                                                <img src="{{ $image['image'] }}" alt=""
                                                    class="w-full h-full object-cover">
                                                <div data-id="{{ $image['id'] }}"
                                                    class="rmImageUpdate absolute hidden group-hover:block top-0 left-0 bg-black/50 cursor-pointer w-full h-full">
                                                    <div class="w-full h-full flex items-center justify-center">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14"
                                                            height="14" fill="currentColor"
                                                            class="text-white hover:text-red-500" viewBox="0 0 16 16">
                                                            <path
                                                                d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5" />
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{-- AVATAR --}}
                        <div class="text-sm w-[calc(100%/2-10px)] flex flex-col gap-3">
                            <label class="w-full flex flex-col gap-1 cursor-pointer">
                                <span class="font-medium mb-1">Image</span>
                                <input id="changeThumbAvatar" type="file" name="file"
                                    class="w-full border rounded-sm px-2 py-1 bg-white">
                            </label>
                            <div class="">
                                <img src="" alt="" class="img-thumbnail max-h-[96px]" id="image-preview"
                                    data-ijabo-default-img="{{ isset($item) ? $item->thumb : '' }}">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="mt-5 flex items-center gap-3 justify-end">
                <a href="{{ route('admin.' . $prefix . '.index') }}"
                    class="py-2 px-4 bg-gray-200 rounded-sm hover:opacity-75" type="button">Cancel</a>
                <button class="py-2 px-4 bg-gray-800 text-yellow-400 rounded-sm hover:opacity-75">
                    {{ isset($item) ? 'Update' : 'Create' }}
                </button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script src="{{ asset('assets/be/js/create/status.js') }}"></script>
    <script src="{{ asset('vendor/ijabo-image/viewer.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/ijaboViewer.js') }}"></script>
    {{-- <script src="{{ asset('vendor/select2/select2.min.js') }}"></script> --}}

    {{-- CKEDITOR --}}
    <script>
        $('.ck-editor').each(function() {
            CKEDITOR.replace(this, {
                filebrowserUploadMethod: "form",
                filebrowserUploadUrl: "{{ route('image.upload', ['_token' => csrf_token()]) }}"
            });
        });
    </script>

    {{-- CONVERT NAME TO SLUG --}}
    <script>
        $(function() {
            $('input[name="name"]').on('keyup', function() {
                const slug = stringToSlug(this.value);
                $('input[name="slug"]').val(slug);
            });
        });
    </script>

    {{-- PREVIEW MULTIPLE IMAGES --}}
    <script>
        document.getElementById('multipleProduct').addEventListener('change', function() {
            const files = this.files;
            const _this = this;
            const wrapImgEl = $('#upload__img-wrap');
            wrapImgEl.empty();
            const dataTransfer = new DataTransfer();
            wrapImgEl.innerHtml = '';

            if (files.length > 30) return;

            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                let fileReader = new FileReader();
                fileReader.onload = (event) => {
                    const html =
                        `<div class="relative w-24 h-24 border border-blue-500 group">
                        <img src="${event.target.result}" class="w-full h-full object-cover">
                        <div data-name="${file.name}" class="rmPreviewImg absolute hidden group-hover:block top-0 left-0 bg-black/50 cursor-pointer w-full h-full">
                            <div class="w-full h-full flex items-center justify-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="text-white hover:text-red-500" viewBox="0 0 16 16">
                                    <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5"/>
                                </svg>
                            </div>
                        </div>
                    </div>`;
                    wrapImgEl.append(html);
                }
                dataTransfer.items.add(file);
                fileReader.readAsDataURL(file);
            }

            $(document).on('click', '.rmPreviewImg', function() {
                const fileName = $(this).data('name');
                $(this).parent().remove();
                for (let x = 0; x < dataTransfer.items.length; x++) {
                    if (fileName == dataTransfer.files[x].name) {
                        dataTransfer.items.remove(x);
                    }
                }
                _this.files = dataTransfer.files;
            });
        });
    </script>

    {{-- REMOVE UPDATE --}}
    <script>
        $(function() {
            $(document).on('click', '.rmImageUpdate', function() {
                const id = $(this).data('id');
                const url = `/admin/product-image/destroy/${id}`;
                const _this = $(this);
                const msgConfirm = confirm('Bạn có chắc xóa ảnh này?');
                if (!msgConfirm) return;

                $.ajax({
                    type: "POST",
                    url,
                    data: {},
                    dataType: "json",
                    success: function(res) {
                        if (res.code == 200) {
                            _this.parent().remove();
                        }
                    }
                });
            });
        });
    </script>

    {{-- SELECT ATTRIBUTE --}}
    <script>
        $(function() {
            const attributeValue = $('#attributeValue');
            const oldAttributeId = "{{ old('attribute_id', $item->attribute_id ?? '') ?? '' }}";
            const oldProductAttributePrice = @json(old('productAttributePrices', $productAttributePrices ?? []) ?? []);
            const oldProductAttributeQuantities = @json(old('productAttributeQuantities', $productAttributeQuantities ?? []) ?? []);

            let oldAttributeValueIds = @json(old('attributeValues', $attributeValues ?? []) ?? []);
            oldAttributeValueIds = oldAttributeValueIds.map((value) => value + '');

            $(document).on('change', 'select#attribute', function() {
                const id = this.value;
                const url = `/admin/attributes/${id}/value`;
                attributeValue.empty();
                if (!id) return;

                $.ajax({
                    type: 'GET',
                    url,
                    data: {},
                    dataType: "json",
                    success: function(res) {
                        if (res.code == 200) {
                            const data = res.data;

                            for (const item of data) {
                                // console.log(oldAttributeValueIds.includes(item.id), item.id, oldAttributeValueIds);
                                const html = `
                                    <div class="flex items-center text-sm gap-3">
                                        <div class="w-1/3 flex items-center gap-1">
                                            <span>Size:</span>
                                            <label class="w-full p-2 bg-white cursor-pointer rounded-sm border flex items-center gap-2 hover:border-blue-500">
                                                <input type="checkbox" value="${item.id}" ${oldAttributeValueIds.includes(`${item.id}`) ? 'checked' : ''} name="attributeValues[]">
                                                <span>${item.name}</span>
                                            </label>
                                        </div>
                                        <div class="w-1/3 flex items-center gap-1">
                                            <span>Price:</span>
                                            <input type="number" value="${oldProductAttributePrice[item.id] || '0'}" name="productAttributePrices[${item.id}]" class="w-full p-2 bg-white outline-none rounded-sm border flex items-center gap-2 hover:border-blue-500" placeholder="Price">
                                        </div>
                                        <div class="w-1/3 flex items-center gap-1">
                                            <span>Quantity:</span>
                                            <input type="number" value="${oldProductAttributeQuantities[item.id] || '0'}" name="productAttributeQuantities[${item.id}]" class="w-full p-2 bg-white outline-none rounded-sm border flex items-center gap-2 hover:border-blue-500" placeholder="Quantity">
                                        </div>
                                    </div>
                                `;
                                attributeValue.append(html);
                            }
                        }
                    }
                });
            });

            if (oldAttributeId) {
                $('select#attribute').val(oldAttributeId).trigger('change');
            }
        });
    </script>

    {{-- CATEGORIES --}}
    <script>
        $(function() {
            const categoryBox = $('.categoryBox');

            categoryBox.each(function() {
                updateCheckAll();
            });

            $(document).on('change', '.selectAllCategories', function() {
                categoryBox.prop('checked', $(this).prop('checked'));
            });

            $(document).on('change', '.categoryBox', function() {
                updateCheckAll();
            });

            function updateCheckAll() {
                const categoryBoxChecked = $('.categoryBox:checked');
                const checkAll = $('.selectAllCategories');

                checkAll.prop('checked', categoryBoxChecked.length == categoryBox.length);
            }
        });
    </script>

    {{-- TYPE CHANGE --}}
    <script>
        $(function() {
            $('input[name="type_id"]').on('change', function() {
                const prefix = $(this).data('prefix');
                const skuInput = $('input[name="sku"]');
                const ranNumber = Math.floor(Math.random() * (10000000 - 10000 + 1)) + 10000;
                skuInput.val(`${prefix}000${ranNumber}`);
            });
        });
    </script>

    {{-- <script>
        console.log();
        $('select[name="categories"]').select2({
            tags: true,
            // tokenSeparators: [',', ' '],
            allowClear: true,
            placeholder: "Select categories...",
            data: @json($categories)
        });
    </script> --}}
@endpush
