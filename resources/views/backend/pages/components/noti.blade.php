@if (session()->has('msg') || session()->has('err'))
    <div class="w-full {{ session()->has('msg') ? 'bg-green-500' : 'bg-red-500' }} px-4 py-1 text-sm text-white rounded-sm mb-2">
        <span>
            {{ session('msg') ?? session('err') }}
        </span>
    </div>
@endif
