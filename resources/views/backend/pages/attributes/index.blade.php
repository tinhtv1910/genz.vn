@extends('backend.layouts.page')

@section('title', $title)

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        {{-- NOTIFICATION --}}
        @include('backend.pages.components.noti')

        <div class="w-full flex gap-5">
            <form action="{{ route('admin.' . $prefix . '.store') }}" method="POST" enctype="multipart/form-data"
                class="w-1/2 block">
                @csrf
                <div class="flex flex-col gap-5">
                    <fieldset class="px-5 py-2 border bg-gray-50 rounded-sm">
                        <legend class="text-xs font-medium text-gray-500">
                            Attribute Info
                        </legend>
                        <div class="flex flex-wrap gap-3">
                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Unit
                                </div>
                                <input type="text" placeholder="Enter unit: cm, mm.." autocomplete="off"
                                    name="short_name" value="{{ old('short_name', $item->short_name ?? '') }}"
                                    class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                @error('short_name')
                                    <div class="mt-1">
                                        <span class="text-sm text-red-500">
                                            {{ $message }}
                                        </span>
                                    </div>
                                @enderror
                            </div>
                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Name
                                </div>
                                <input type="text" placeholder="Enter name" autocomplete="off" name="name"
                                    value="{{ old('name', $item->name ?? '') }}"
                                    class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                @error('name')
                                    <div class="mt-1">
                                        <span class="text-sm text-red-500">
                                            {{ $message }}
                                        </span>
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="mt-5 flex items-center gap-3 justify-end">
                            <button class="py-2 px-4 bg-gray-800 text-yellow-400 rounded-sm hover:opacity-75">
                                {{ isset($item) ? 'Update' : 'Create' }}
                            </button>
                        </div>
                    </fieldset>
                </div>
            </form>

            <form action="{{ route('admin.attributes.value') }}" method="POST" class="w-1/2 block">
                @csrf
                <div class="w-full flex flex-col gap-5">
                    <fieldset class="px-5 py-2 border bg-gray-50 rounded-sm">
                        <legend class="text-xs font-medium text-gray-500">
                            Attribute value
                        </legend>
                        <div class="flex flex-wrap gap-3">
                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Attribute
                                </div>
                                <select name="attribute_id"
                                    class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                    <option value="" selected>
                                        Select attribute name
                                    </option>
                                    @foreach ($data as $attribute)
                                        <option value="{{ $attribute->id }}" {{ old('attribute_id') }}>
                                            {{ $attribute->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('attribute_id')
                                    <div class="mt-1">
                                        <span class="text-sm text-red-500">
                                            {{ $message }}
                                        </span>
                                    </div>
                                @enderror
                            </div>

                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Value
                                </div>
                                <input type="text" placeholder="Enter value" autocomplete="off" name="value"
                                    value="{{ old('value') }}"
                                    class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                @error('value')
                                    <div class="mt-1">
                                        <span class="text-sm text-red-500">
                                            {{ $message }}
                                        </span>
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="mt-5 flex items-center gap-3 justify-end">
                            <button class="py-2 px-4 bg-gray-800 text-yellow-400 rounded-sm hover:opacity-75">
                                {{ isset($item) ? 'Update' : 'Create' }}
                            </button>
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>

        <div class="mt-10">
            <table class="border-collapse w-full text-sm">
                <thead>
                    <th class="border bg-gray-50 uppercase p-2 text-center">
                        <input type="checkbox" id="mainCheckbox">
                    </th>
                    <th class="border bg-gray-50 uppercase p-2 text-center">#</th>
                    <th class="border bg-gray-50 uppercase p-2">Unit</th>
                    <th class="border bg-gray-50 uppercase p-2">Name</th>
                    <th class="border bg-gray-50 uppercase p-2">Value</th>
                    <th class="border bg-gray-50 uppercase p-2"></th>
                </thead>
                <tbody>
                    @forelse ($data as $item)
                        <tr>
                            <td class="border p-2 text-center">
                                <input type="checkbox" id="checkboxItem" value="{{ $item->id }}">
                            </td>
                            <td class="border p-2 text-center">
                                {{ $item->id }}
                            </td>
                            <td class="border p-2">
                                <input id="attrShortName{{ $item->id }}" type="text" data-id="{{ $item->id }}"
                                    name="short_name" value="{{ $item->short_name ?? '-' }}"
                                    placeholder="{{ $item->short_name ?? '-' }}" class="p-2 border outline-none">
                            </td>
                            <td class="border p-2">
                                <input id="attrName{{ $item->id }}" type="text" data-id="{{ $item->id }}"
                                    name="name" value="{{ $item->name }}" placeholder="{{ $item->name }}"
                                    class="p-2 border outline-none">
                            </td>
                            <td class="w-1/3 border p-2 group">
                                <div class="flex flex-wrap gap-1">
                                    @forelse ($item->attributeValues as $value)
                                        <input data-id="{{ $value->id }}" type="text"
                                            class="inputAttributeValue w-[calc(100%/4-10px)] px-4 py-1 outline-none border"
                                            value="{{ $value->name }}">
                                    @empty
                                        -
                                    @endforelse
                                </div>
                            </td>
                            <td class="border p-2">
                                <div class="flex flex-wrap justify-center gap-1">
                                    <a href="{{ route('admin.' . $prefix . '.edit', $item->id) }}"
                                        class="bg-blue-700 rounded-sm w-8 h-7 flex items-center justify-center text-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                            fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.5.5 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11z" />
                                        </svg>
                                    </a>
                                    <a href="#"
                                        onclick="event.preventDefault(); return confirm('Are you sure to delete item {{ $item->name }}?') ? document.getElementById('deleteForm{{ $item->id }}').submit() : false"
                                        class="bg-red-500 rounded-sm w-8 h-7 flex items-center justify-center text-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                            fill="currentColor" class="bi bi-trash2-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M2.037 3.225A.7.7 0 0 1 2 3c0-1.105 2.686-2 6-2s6 .895 6 2a.7.7 0 0 1-.037.225l-1.684 10.104A2 2 0 0 1 10.305 15H5.694a2 2 0 0 1-1.973-1.671zm9.89-.69C10.966 2.214 9.578 2 8 2c-1.58 0-2.968.215-3.926.534-.477.16-.795.327-.975.466.18.14.498.307.975.466C5.032 3.786 6.42 4 8 4s2.967-.215 3.926-.534c.477-.16.795-.327.975-.466-.18-.14-.498-.307-.975-.466z" />
                                        </svg>
                                    </a>

                                    <form id="deleteForm{{ $item->id }}" class="hidden"
                                        action="{{ route('admin.' . $prefix . '.destroy', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="20" class="p-3 border">[Empty item]</td>
                        </tr>
                    @endforelse
                </tbody>
                @if ($data->count())
                    <tfoot>
                        <tr>
                            <td colspan="20" class="border p-2">
                                <div>{{ $data->links('pagination::tailwind') }}</div>
                            </td>
                        </tr>
                    </tfoot>
                @endif
            </table>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('assets/be/js/index/detectClickEvent.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/toggleStatusUpdate.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/elementDefine.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/processCheckboxItem.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/deleteMultiItems.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/updateStatusRecord.js') }}"></script>
    <script>
        $(function() {
            $(document).on('change', 'input.inputAttributeValue', function() {
                const id = $(this).data('id');
                const val = $(this).val();
                const url = "{{ route('admin.attributes.value.update') }}";
                $.ajax({
                    type: 'POST',
                    url,
                    data: {
                        id,
                        val
                    },
                    success: function(res) {
                        alert(res.msg);
                    }
                });
            });

            $(document).on('change', 'input[name="short_name"], input[name="name"]', function() {
                const id = $(this).data('id');
                const elShortName = $(`#attrShortName${id}`);
                const elName = $(`#attrName${id}`);
                const url = `/admin/attributes/${id}`;
                const data = {
                    id
                };

                if (elName.val()) data.name = elName.val();
                if (elShortName.val()) data.short_name = elShortName.val();

                $.ajax({
                    type: 'PATCH',
                    url,
                    data,
                    success: function(res) {
                        console.log(res.msg);
                    }
                });
            });
        });
    </script>
@endpush
