@extends('backend.layouts.page')

@section('title', $title)

@push('style')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
@endpush

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <a href="{{ route('admin.' . $prefix . '.index') }}" class="hover:text-blue-500">
                {{ $parentBreadcrumb }}
            </a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        {{-- NOTI --}}
        @include('backend.pages.components.noti')

        {{-- FORM --}}
        <form
            action="{{ isset($user) ? route('admin.' . $prefix . '.update', $user->id) : route('admin.' . $prefix . '.store') }}"
            method="POST" enctype="multipart/form-data">
            @csrf
            @if (isset($user))
                @method('PUT')
                <input type="hidden" name="id" class="hidden" value="{{ $user->id }}">
            @else
                @method('POST')
            @endif
            <div class="w-full flex gap-5">
                <fieldset class="w-full px-5 py-2 border bg-gray-50 rounded-sm">
                    <legend class="text-xs font-medium text-gray-500">
                        Infomation
                    </legend>
                    <div class="w-full flex flex-wrap justify-between gap-y-3">
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Email <span class="text-red-500">(*)</span>
                            </div>
                            <input type="email" placeholder="Enter email" name="email"
                                value="{{ old('email', $user->email ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('email')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Name <span class="text-red-500">(*)</span>
                            </div>
                            <input type="text" placeholder="Enter name" name="name"
                                value="{{ old('name', $user->name ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('name')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Select role
                            </div>
                            <select name="role_id"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                <option value="" selected>--Select user role--</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}"
                                        {{ old('role_id', $user->role_id ?? '') == $role->id ? 'selected' : '' }}>
                                        {{ $role->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Date of Birth
                            </div>
                            <input type="date" name="dob" value="{{ old('dob', $user->dob ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">

                            @error('dob')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        @if (!isset($user))
                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Password <span class="text-red-500">(*)</span>
                                </div>
                                <input type="password" name="password" placeholder="••••••"
                                    class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                @error('password')
                                    <div class="mt-1">
                                        <span class="text-sm text-red-500">
                                            {{ $message }}
                                        </span>
                                    </div>
                                @enderror
                            </div>

                            <div class="text-sm w-[calc(100%/2-10px)]">
                                <div class="font-medium mb-1">
                                    Confirm password <span class="text-red-500">(*)</span>
                                </div>
                                <input type="password" name="confirm_password" placeholder="••••••"
                                    class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                @error('confirm_password')
                                    <div class="mt-1">
                                        <span class="text-sm text-red-500">
                                            {{ $message }}
                                        </span>
                                    </div>
                                @enderror
                            </div>
                        @endif

                        <div class="text-sm w-full flex flex-col gap-3">
                            <label class="w-full flex flex-col gap-1">
                                <span class="font-medium mb-1">Avatar</span>
                                <span
                                    class="w-full bg-white border p-5 rounded-sm border-dashed cursor-pointer hover:border-gray-800 flex flex-col gap-1 items-center justify-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        fill="currentColor" class="text-gray-400" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M7.646 5.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 6.707V10.5a.5.5 0 0 1-1 0V6.707L6.354 7.854a.5.5 0 1 1-.708-.708z" />
                                        <path
                                            d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383m.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z" />
                                    </svg>
                                    <span class="text-gray-500">Upload image from device</span>
                                </span>
                                <input id="changeThumbAvatar" type="file" name="file" class="hidden">
                            </label>
                            <div class="">
                                <img src="" alt="" class="img-thumbnail max-h-[200px]" id="image-preview"
                                    data-ijabo-default-img="{{ isset($user) ? $user->thumb : '' }}">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="flex gap-5 mt-10">
                <fieldset class="w-full px-5 py-2 border bg-gray-50 rounded-sm">
                    <legend class="text-xs font-medium text-gray-500">
                        Contact
                    </legend>
                    <div class="flex flex-wrap w-full justify-between gap-y-3">
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Select province
                            </div>
                            <select name="province_id" data-url="{{ route('ajax.address') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                <option value="0" selected>--Select province--</option>
                                @forelse ($provinces as $province)
                                    <option value="{{ $province->id }}"
                                        {{ old('province_id', $user->province_id ?? '') == $province->id ? 'selected' : '' }}>
                                        {{ $province->name }}
                                    </option>
                                @empty
                                    <option value="0">--Select province--</option>
                                @endforelse
                            </select>
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Select district
                            </div>
                            <select name="district_id" data-url="{{ route('ajax.address') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                <option value="0" selected>--Select district--</option>
                            </select>
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Select ward
                            </div>
                            <select name="ward_id"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                <option value="0" selected>--Select ward--</option>
                            </select>
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Detail Address
                            </div>
                            <input type="text" placeholder="Enter address" name="address"
                                value="{{ old('address', $user->address ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Phone
                            </div>
                            <input type="text" placeholder="Enter phone" name="phone"
                                value="{{ old('phone', $user->phone ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                        </div>

                        <div class="text-sm flex items-end gap-10 w-[calc(100%/2-10px)]">
                            <div class="font-medium">
                                Active:
                            </div>

                            <div class="flex gap-3">
                                <label class="cursor-pointer border rounded-sm px-4 py-1">
                                    <input type="radio" class="status" id="block-button" hidden value="0"
                                        {{ isset($user) && $user->status == 0 ? 'checked' : '' }} name="status">
                                    <span>OFF</span>
                                </label>

                                <label class="cursor-pointer border rounded-sm px-4 py-1">
                                    <input type="radio" class="status" id="active-button" hidden value="1"
                                        {{ (isset($user) && $user->status == 1) || !isset($user) ? 'checked' : '' }}
                                        name="status">
                                    <span>ON</span>
                                </label>
                            </div>
                        </div>

                        <div class="text-sm w-full">
                            <div class="font-medium mb-1">
                                Note
                            </div>
                            <textarea name="description" rows="5" class="w-full p-2 outline-none rounded-sm border focus:border-gray-800"
                                placeholder="Note">{{ old('description', $user->description ?? '') }}</textarea>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="mt-5 flex items-center gap-3 justify-end">
                <a href="{{ route('admin.' . $prefix . '.index') }}"
                    class="py-2 px-4 bg-gray-200 rounded-sm hover:opacity-75" type="button">Cancel</a>
                <button class="py-2 px-4 bg-gray-800 text-yellow-400 rounded-sm hover:opacity-75">
                    {{ isset($user) ? 'Update' : 'Create' }}
                </button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/create/status.js') }}"></script>
    <script src="{{ asset('vendor/ijabo-image/viewer.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/ijaboViewer.js') }}"></script>
    <script>
        $(function() {
            const provinceEl = $('select[name="province_id"]');
            const districtEl = $('select[name="district_id"]');
            const wardEl = $('select[name="ward_id"]');
            const provinceId = "{{ old('province_id', $user->province_id ?? '') }}";
            const districtId = "{{ old('district_id', $user->district_id ?? '') }}";
            const wardId = "{{ old('ward_id', $user->ward_id ?? '') }}";

            provinceEl.select2({
                placeholder: "--Select province--",
            });

            districtEl.select2({
                placeholder: "--Select district--",
            });

            wardEl.select2({
                placeholder: "--Select ward--",
            });

            provinceEl.on("change", function() {
                const id = this.value;
                const _this = $(this);
                const url = _this.data('url');

                if (!id || id == 0) {
                    districtEl.children().remove();
                    wardEl.children().remove();
                    return;
                }

                $.ajax({
                    type: "POST",
                    url,
                    dataType: "json",
                    data: {
                        id,
                    },
                    success: function(res) {
                        console.log(res);
                        if (res.code === 200) {
                            const html = res.data;
                            districtEl.children().remove();
                            districtEl.append(html);

                            if (districtId) {
                                districtEl.val(districtId).trigger("change");
                            }
                        }
                    },
                });
            });

            districtEl.on("change", function() {
                const id = this.value;
                const _this = $(this);
                const url = _this.data('url');

                if (!id || id == 0) {
                    wardEl.children().remove();
                    return;
                }

                $.ajax({
                    type: "POST",
                    url,
                    dataType: "json",
                    data: {
                        id,
                        type: 2,
                    },
                    success: function(res) {
                        console.log(res);
                        if (res.code === 200) {
                            const html = res.data;
                            wardEl.children().remove();
                            wardEl.append(html);

                            if (wardId) {
                                wardEl.val(wardId).trigger("change");
                            }
                        }
                    },
                });
            });

            if (provinceId) {
                provinceEl.val(provinceId).trigger("change");
            }
        });
    </script>
@endpush
