<div class="mb-2">
    <form action="{{ route('admin.'.$prefix.'.index') }}" method="GET">
        <fieldset class="border rounded-sm p-2 text-sm">
            <legend class="text-gray-500">Filter</legend>
            <div class="flex items-center justify-between">
                <div>
                    <select class="border rounded-sm py-1 px-2 outline-none" name="limit" id="">
                        <option value="25" selected>Options</option>
                        @foreach (options() as $option)
                            <option value="{{ $option }}" {{ request('limit') == $option ? 'selected' : '' }}>
                                {{ $option }}
                            </option>
                        @endforeach
                        <option value="all">Show all</option>
                    </select>
                </div>
                <div class="flex flex-wrap gap-2 justify-end">
                    <select class="border rounded-sm py-1 px-2 outline-none" name="status" id="">
                        <option value="" selected>--Status--</option>
                        <option value="1" {{ request('status') == "1" ? 'selected' : '' }}>Active</option>
                        <option value="0" {{ request('status') == "0" ? 'selected' : '' }}> Non-active</option>
                    </select>

                    <select class="border rounded-sm py-1 px-2 outline-none" name="role_id" id="">
                        <option value="" selected>--Role--</option>
                        @foreach ($roles as $role)
                            <option {{ request('role_id') == $role->id ? 'selected' : '' }} value="{{ $role->id }}">
                                {{ $role->name }}
                            </option>
                        @endforeach
                    </select>

                    <input type="search" value="{{ request('search') ?? old('search') }}" name="search"
                        class="border rounded-sm focus:outline-0 py-1 px-4" placeholder="Search...">

                    <button type="submit"
                        class="px-5 hover:opacity-70 py-1 text-white rounded-sm bg-gray-800">Search</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
