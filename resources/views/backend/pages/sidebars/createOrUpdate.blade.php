@extends('backend.layouts.page')

@section('title', $title)

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <a href="{{ route('admin.' . $prefix . '.index') }}" class="hover:text-blue-500">
                {{ $parentBreadcrumb }}
            </a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        {{-- NOTIFICATION --}}
        @include('backend.pages.components.noti')

        <form
            action="{{ isset($item) ? route('admin.' . $prefix . '.update', $item->id) : route('admin.' . $prefix . '.store') }}"
            method="POST" enctype="multipart/form-data">
            @csrf
            @if (isset($item))
                @method('PUT')
                <input type="hidden" name="id" class="hidden" value="{{ $item->id }}">
            @else
                @method('POST')
            @endif
            <div class="flex gap-5">
                <fieldset class="w-full px-5 py-2 border bg-gray-50 rounded-sm">
                    <legend class="text-xs font-medium text-gray-500">
                        Infomation
                    </legend>
                    <div class="flex flex-wrap gap-3">
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Select Level
                            </div>
                            <select name="parent_id"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                <option value="0" {{ old('parent_id', $item->parent_id ?? '') ? 'selected' : '' }}>
                                    --Default level--
                                </option>
                                {!! $sidebarOptions !!}
                            </select>
                            @error('parent_id')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Sidebar type
                            </div>
                            <select name="type_id"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                                <option value="0" {{ old('type_id', $item->type_id ?? '') ? 'selected' : '' }}>
                                    Sidebar (default)
                                </option>
                                <option value="1" {{ old('type_id', $item->type_id ?? '') ? 'selected' : '' }}>
                                    Line
                                </option>
                            </select>
                            @error('type_id')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Name
                            </div>
                            <input type="text" placeholder="Enter name" name="name"
                                value="{{ old('name', $item->name ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('name')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Slug
                            </div>
                            <input type="text" placeholder="Enter slug" name="slug"
                                value="{{ old('slug', $item->slug ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('slug')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Order
                            </div>
                            <input type="number" placeholder="Order menu" name="order"
                                value="{{ old('order', $item->order ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('order')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="text-sm w-full">
                            <div class="font-medium mb-1">
                                Icon (svg)
                            </div>
                            <textarea name="icon" rows="8" placeholder="Svg icon"
                                class="w-full px-2 outline-none rounded-sm border focus:border-gray-800">{{ old('icon', $item->icon ?? '') }}</textarea>
                            <div class="text-sm flex gap-1">
                                <span>Tham khảo tại:</span>
                                <a href="https://icons.getbootstrap.com" target="_blank" class="text-blue-500">
                                    https://icons.getbootstrap.com
                                </a>
                            </div>
                            @error('slug')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="mt-5 flex items-center gap-3 justify-end">
                <a href="{{ route('admin.' . $prefix . '.index') }}"
                    class="py-2 px-4 bg-gray-200 rounded-sm hover:opacity-75" type="button">Cancel</a>
                <button class="py-2 px-4 bg-gray-800 text-yellow-400 rounded-sm hover:opacity-75">
                    {{ isset($item) ? 'Update' : 'Create' }}
                </button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script src="{{ asset('assets/be/js/create/status.js') }}"></script>
@endpush
