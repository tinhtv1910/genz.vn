@extends('backend.layouts.page')

@section('title', $title)

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <a href="{{ route('admin.'.$prefix.'.index') }}" class="hover:text-blue-500">
                {{ $parentBreadcrumb }}
            </a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        {{-- NOTIFICATION --}}
        @include('backend.pages.components.noti')

        <form
            action="{{ isset($item) ? route('admin.'.$prefix.'.update', $item->id) : route('admin.'.$prefix.'.store') }}"
            method="POST" enctype="multipart/form-data">
            @csrf
            @if (isset($item))
                @method('PUT')
                <input type="hidden" name="id" class="hidden" value="{{ $item->id }}">
            @else
                @method('POST')
            @endif
            <div class="flex flex-col gap-10">
                <fieldset class="w-full px-5 py-2 border bg-gray-50 rounded-sm">
                    <legend class="text-xs font-medium text-gray-500">
                        Infomation
                    </legend>
                    <div class="flex flex-wrap gap-3">
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Name
                            </div>
                            <input type="text" placeholder="Enter name" name="name"
                                value="{{ old('name', $item->name ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('name')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Slug
                            </div>
                            <input type="text" placeholder="Enter slug" name="slug"
                                value="{{ old('slug', $item->slug ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('slug')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Order
                            </div>
                            <input type="number" placeholder="Order menu" name="order"
                                value="{{ old('order', $item->order ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('order')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Active:
                            </div>

                            <div class="flex gap-3">
                                <label class="cursor-pointer border rounded-sm px-4 py-1">
                                    <input type="radio" class="status" id="block-button" hidden value="0"
                                        {{ isset($item) && $item->status == 0 ? 'checked' : '' }} name="status">
                                    <span>OFF</span>
                                </label>

                                <label class="cursor-pointer border rounded-sm px-4 py-1">
                                    <input type="radio" class="status" id="active-button" hidden value="1"
                                        {{ (isset($item) && $item->status == 1) || !isset($item) ? 'checked' : '' }}
                                        name="status">
                                    <span>ON</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="w-full px-5 py-2 border bg-gray-50 rounded-sm">
                    <legend class="text-xs font-medium text-gray-500">
                        SEO
                    </legend>
                    <div class="flex flex-wrap gap-3">
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Meta title
                            </div>
                            <input type="text" placeholder="Enter meta title" name="meta_title"
                                value="{{ old('meta_title', $item->meta_title ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('meta_title')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Meta description
                            </div>
                            <input type="text" placeholder="Enter meta description" name="meta_description"
                                value="{{ old('meta_description', $item->meta_description ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('meta_description')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="text-sm w-[calc(100%/2-10px)]">
                            <div class="font-medium mb-1">
                                Meta keywords
                            </div>
                            <input type="text" placeholder="Enter meta title" name="meta_keywords"
                                value="{{ old('meta_keywords', $item->meta_keywords ?? '') }}"
                                class="w-full px-2 h-10 outline-none rounded-sm border focus:border-gray-800">
                            @error('meta_keywords')
                                <div class="mt-1">
                                    <span class="text-sm text-red-500">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="text-sm w-[calc(100%/2-10px)] flex flex-col gap-3">
                            <label class="w-full flex flex-col gap-1">
                                <span class="font-medium mb-1">Meta image</span>
                                <input id="changeThumbAvatar" type="file" name="meta_thumb" class="">
                                <div class="">
                                    <img src="" alt="" class="img-thumbnail max-h-[200px]" id="image-preview"
                                        data-ijabo-default-img="{{ isset($item) ? $item->meta_img : '' }}">
                                </div>
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="mt-5 flex items-center gap-3 justify-end">
                <a href="{{ route('admin.'.$prefix.'.index') }}" class="py-2 px-4 bg-gray-200 rounded-sm hover:opacity-75"
                    type="button">Cancel</a>
                <button class="py-2 px-4 bg-gray-800 text-yellow-400 rounded-sm hover:opacity-75">
                    {{ isset($item) ? 'Update' : 'Create' }}
                </button>
            </div>
        </form>
    </div>

@endsection

@push('script')
    <script src="{{ asset('assets/be/js/create/status.js') }}"></script>
    <script src="{{ asset('vendor/ijabo-image/viewer.min.js') }}"></script>
    <script src="{{ asset('assets/be/js/ijaboViewer.js') }}"></script>
@endpush
