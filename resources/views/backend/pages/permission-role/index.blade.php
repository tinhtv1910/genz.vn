@extends('backend.layouts.page')

@section('title', $title)

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        <div class="border rounded-sm">
            <div class="border-b p-2 bg-gray-50 flex items-center justify-between">
                <span class="uppercase text-xs font-bold text-gray-500">{{ $title }}</span>
                <div class="flex items-center gap-3">
                </div>
            </div>
            <div class="p-2">
                {{-- NOTIFICATION --}}
                @include('backend.pages.components.noti')

                <div class="w-full text-sm">
                    <form action="{{ route('admin.permission-roles.store') }}" method="POST">
                        @csrf
                        @method('POST')
                        <table class="w-full">
                            <thead>
                                <tr>
                                    <th class="p-2 bg-gray-50 text-gray-800">
                                        Permission & route
                                    </th>
                                    @foreach ($roles as $role)
                                        <th class="p-2 bg-gray-50 text-gray-800 text-center">
                                            {{ $role->name }}
                                        </th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="font-semibold text-blue-500">Check all</td>
                                    @foreach ($roles as $role)
                                        <td class="p-2 text-center">
                                            <label
                                                class="border px-2 py-1 rounded-sm w-max mx-auto cursor-pointer flex items-center gap-1">
                                                <input id="main_role_checkbox_{{ $role->id }}" type="checkbox"
                                                    class="mainRoleCheckbox" value="{{ $role->id }}">
                                                <span>Select all</span>
                                            </label>
                                        </td>
                                    @endforeach
                                </tr>
                                @forelse ($permissions as $permission)
                                    <tr>
                                        <td class="p-2">
                                            <div class="flex justify-between">
                                                <div>
                                                    {{ $permission->name }}
                                                </div>
                                                <div>
                                                    <span class="text-gray-500 italic text-xs">
                                                        {{ $permission->slug }}
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                        @foreach ($roles as $role)
                                            <td class="p-2 text-center">
                                                <input class="assignRole{{ $role->id }} assignRoleCheckbox"
                                                    type="checkbox" name="permission[{{ $role->id }}][]"
                                                    {{ isset($dataPermissionRole[$role->id]) && in_array($permission->id, $dataPermissionRole[$role->id]) ? 'checked' : '' }}
                                                    value="{{ $permission->id }}" data-role={{ $role->id }}>
                                            </td>
                                        @endforeach
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="20" class="p-2 border-t">Empty permission</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="w-full mt-5 flex items-center justify-end">
                            <button type="submit" class="px-4 py-2 rounded-sm bg-gray-800 text-yellow-400">
                                Create
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script>
        $(function() {
            const mainRoleCheckBox = $('.mainRoleCheckbox');
            const checkboxItem = $('.assignRoleCheckbox');

            checkboxItem.each(function (item) {
                const roleId = $(this).data('role');
                const checkboxLength = $(`input.assignRole${roleId}`).length;
                const checkedboxLength = $(`input.assignRole${roleId}:checked`).length;
                $('input#main_role_checkbox_' + roleId).prop('checked', checkedboxLength == checkboxLength)
            });

            mainRoleCheckBox.on('click', function() {
                const _this = $(this);
                const roleId = this.value

                $(`input.assignRole${roleId}`).prop('checked', _this.prop('checked'));
            });

            checkboxItem.on('change', function() {
                const _this = $(this);
                const roleId = _this.data('role');
                const checkboxLength = $(`input.assignRole${roleId}`).length;
                const checkedboxLength = $(`input.assignRole${roleId}:checked`).length;

                $('input#main_role_checkbox_' + roleId).prop('checked', checkedboxLength == checkboxLength);
            });
        });
    </script>
@endpush
