<table class="border-collapse w-full text-sm">
    <thead>
        <th class="border bg-gray-50 uppercase p-2 text-center">
            <input type="checkbox" id="mainCheckbox">
        </th>
        <th class="border bg-gray-50 uppercase p-2 text-center">#</th>
        <th class="border bg-gray-50 uppercase p-2">Tracking Id/No</th>
        <th class="border bg-gray-50 uppercase p-2">Payment Mode</th>
        <th class="border bg-gray-50 uppercase p-2">Transport Mode</th>
        <th class="border bg-gray-50 uppercase p-2">Total Price</th>
        <th class="border bg-gray-50 uppercase p-2">Status</th>
        <th class="border bg-gray-50 uppercase p-2">Created At</th>
        <th class="border bg-gray-50 uppercase p-2"></th>
    </thead>
    <tbody>
        @forelse ($data as $item)
            <tr>
                <td class="border p-2 text-center">
                    <input type="checkbox" id="checkboxItem" value="{{ $item->id }}">
                </td>
                <td class="border p-2 text-center">
                    {{ $item->id }}
                </td>
                <td class="border p-2">
                    {{ $item->tracking_no }}
                </td>
                <td class="border p-2">
                    {{ $item->payment_mode }}
                </td>
                <td class="border p-2">
                    {{ $item->transport_mode }}
                </td>
                <td class="border p-2">
                    {{ number_format($item->total_price, 0, ',', '.') }} VNĐ
                </td>
                <td class="border p-2">
                    <div>
                        <span style="color: {{ $item->orderStatus->color }}">
                            {{ $item->orderStatus->name ?? '-' }}
                        </span>
                    </div>
                </td>
                <td class="border p-2">
                    {{ $item->created_at }}
                </td>
                <td class="border p-2">
                    <div class="flex flex-wrap justify-center gap-1">
                        <a href="{{ route('admin.' . $prefix . '.show', $item->id) }}"
                            class="bg-blue-700 rounded-sm w-8 h-7 flex items-center justify-center text-white">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-eye-fill" viewBox="0 0 16 16">
                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0" />
                                <path
                                    d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8m8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7" />
                            </svg>
                        </a>
                        <span class="hidden">
                            <a href="#"
                                onclick="event.preventDefault(); return confirm('Are you sure to delete item {{ $item->name }}?') ? document.getElementById('deleteForm{{ $item->id }}').submit() : false"
                                class="bg-red-500 rounded-sm w-8 h-7 flex items-center justify-center text-white">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                    fill="currentColor" class="bi bi-trash2-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M2.037 3.225A.7.7 0 0 1 2 3c0-1.105 2.686-2 6-2s6 .895 6 2a.7.7 0 0 1-.037.225l-1.684 10.104A2 2 0 0 1 10.305 15H5.694a2 2 0 0 1-1.973-1.671zm9.89-.69C10.966 2.214 9.578 2 8 2c-1.58 0-2.968.215-3.926.534-.477.16-.795.327-.975.466.18.14.498.307.975.466C5.032 3.786 6.42 4 8 4s2.967-.215 3.926-.534c.477-.16.795-.327.975-.466-.18-.14-.498-.307-.975-.466z" />
                                </svg>
                            </a>
                        </span>

                        <form id="deleteForm{{ $item->id }}" class="hidden"
                            action="{{ route('admin.' . $prefix . '.destroy', $item->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                        </form>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="20" class="p-3 border">[Empty item]</td>
            </tr>
        @endforelse
    </tbody>
    @if ($data->count())
        <tfoot>
            <tr>
                <td colspan="20" class="border p-2">
                    <div>{{ $data->links('pagination::tailwind') }}</div>
                </td>
            </tr>
        </tfoot>
    @endif
</table>
