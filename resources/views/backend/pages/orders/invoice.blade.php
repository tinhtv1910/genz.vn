<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, viewport-fit=cover, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="_token" content="{{ csrf_token() }}" />

    <title>@yield('title')Invoice | heji.vn</title>
    <link rel="shortcut icon" href="" type="image/x-icon">
    <style>
        * {
            font-family: "DejaVu Sans", sans-serif;
        }

        table {
            border-collapse: collapse;
            font-family: "DejaVu Sans", sans-serif;
        }

        table th {
            border: 1px solid rgb(56, 56, 56);
            padding: 8px;
            background: rgb(56, 56, 56);
            color: white;
            text-align: left;
            font-weight: 600;
            text-transform: uppercase;
        }

        .invoice-header {
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            border-bottom: 1px dashed #ccc;
            padding-bottom: 8px;
        }

        .invoice-column {
            border: 1px solid #ccc;
            padding: 8px;
        }

        .page-break {
            page-break-after: always;
            margin: 20px;
        }
    </style>
</head>

<body class="">

    <section></section>

    <section>
        <div style="padding: 40px; margin: auto;">
            <div>
                <table style="width: 100%; font-size: 14px;">
                    <thead>
                        <tr>
                            <td colspan="2"
                                style="border: 1px solid #ccc; font-size: 30px; padding: 8px;">
                                Heji.vn
                            </td>
                            <td colspan="2" style="padding: 8px; border: 1px solid #ccc;">
                                <span class="" style="width: 100%; display: flex; flex-direction: column; gap: 8px;">
                                    <span class="invoice-header">
                                        <span style=""> Invoice ID: </span>
                                        <span>#{{ $item->id }}</span>
                                    </span>
                                    <span class="invoice-header">
                                        <span style=""> Date: </span>
                                        <span>{{ date('d-m-Y') }}</span>
                                    </span>
                                    <span class="invoice-header">
                                        <span style=""> Store Adress: </span>
                                        <span>Kim giang, Dai kim, Hoang Mai, Ha Noi</span>
                                    </span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">Order detail</th>
                            <th colspan="2">User Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="invoice-column">Order ID</td>
                            <td class="invoice-column">{{ $item->id }}</td>
                            <td class="invoice-column">Name</td>
                            <td class="invoice-column">{{ $item->name }}</td>
                        </tr>
                        <tr>
                            <td class="invoice-column">Tracking No/ID</td>
                            <td class="invoice-column">{{ $item->tracking_no }}</td>
                            <td class="invoice-column">Email</td>
                            <td class="invoice-column">{{ $item->email ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td class="invoice-column">Created Order</td>
                            <td class="invoice-column">{{ $item->created_at }}</td>
                            <td class="invoice-column">Phone</td>
                            <td class="invoice-column">{{ $item->phone }}</td>
                        </tr>
                        <tr>
                            <td class="invoice-column">Payment Mode</td>
                            <td class="invoice-column">{{ $item->payment_mode }}</td>
                            <td class="invoice-column">Address</td>
                            <td class="invoice-column">
                                {{ $item->address }} {{ $item->ward->name ?? '' }} {{ $item->district->name ?? '' }}
                                {{ $item->province->name ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td class="invoice-column">Transport Mode</td>
                            <td class="invoice-column">{{ $item->transport_mode }}</td>
                            <td class="invoice-column">Date of Birth</td>
                            <td class="invoice-column">{{ $item->dob ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td class="invoice-column">Status</td>
                            <td class="invoice-column">{{ $item->orderStatus->name ?? '-' }}</td>
                            <td class="invoice-column">Gender</td>
                            <td class="invoice-column">{{ $item->gender == '1' ? 'Male' : 'Female' }}</td>
                        </tr>
                        <tr>
                            <td class="invoice-column" style="" colspan="4">
                                <div>Note</div>
                                <div style="width: 100%; box-sizing:  border-box;">
                                    <textarea rows="3" placeholder="User node" readonly
                                        style="margin-top: 4px; width: calc(100% - 20px); padding: 8px; max-width: 100%; border: 1px solid #ccc; outline: none; resize: none">{{ $item->note }}</textarea>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="page-break"></div>
            
            <div>
                <table style="width: 100%; font-size: 14px;">
                    <thead>
                        <tr>
                            <td style="border: 1px solid #ccc; padding: 8px; font-weight: 500; font-size: 22px;"
                                colspan="7">Products</td>
                        </tr>
                        <tr class="" style="background: rgb(56, 56, 56); color: white;">
                            <td class="invoice-column" style="border: 1px solid rgb(56, 56, 56)">#</td>
                            <td class="invoice-column" style="border: 1px solid rgb(56, 56, 56)">Thumb
                            </td>
                            <td class="invoice-column" style="border: 1px solid rgb(56, 56, 56)">Product
                            </td>
                            <td class="invoice-column" style="border: 1px solid rgb(56, 56, 56)">Attribute
                            </td>
                            <td class="invoice-column" style="border: 1px solid rgb(56, 56, 56)">Quantity
                            </td>
                            <td class="invoice-column" style="border: 1px solid rgb(56, 56, 56)">Price
                            </td>
                            <td class="invoice-column" style="border: 1px solid rgb(56, 56, 56)">Total
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($item->orderDetails as $order)
                            <tr>
                                <td class="invoice-column">{{ $order->id }}</td>
                                <td class="" style="border: 1px solid #ccc; text-align: center">
                                    <img src="{{ $order->product->thumb ?? '' }}" width="40" height="40"
                                        alt="">
                                </td>
                                <td class="invoice-column">{{ $order->product->name ?? '' }}</td>
                                <td class="invoice-column">{{ $order->attributeValue->name ?? '-' }}
                                    {{ $order->attributeValue->attribute->short_name ?? '' }}</td>
                                <td class="invoice-column">{{ $order->quantity }}</td>
                                <td class="invoice-column">{{ number_format($order->price, 0, ',', '.') }} VNĐ</td>
                                <td class="invoice-column">
                                    {{ number_format($order->quantity * $order->price, 0, ',', '.') }}
                                    VNĐ</td>
                            </tr>
                        @empty
                        @endforelse
                        <tr class="invoice-column">
                            <td class="" colspan="6" style="padding: 8px;">
                                <span class="" style="font-size: 16px;">
                                    Total (price include VAT)
                                </span>
                            </td>
                            <td class="" style="text-align: right; padding: 8px;">
                                <span style="font-size: 24px;">
                                    {{ number_format($item->total_price, 0, ',', '.') }} VNĐ
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="" style="margin: 20px auto; text-align: center">
                <span>
                    Thank you for shopping <b>HEJI</b>
                </span>
            </div>
        </div>
    </section>

    <section></section>
    @stack('script')
</body>

</html>
