@extends('backend.layouts.page')

@section('title', $title)

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        <div class="border rounded-sm">
            <div class="border-b p-2 bg-gray-50 flex items-center justify-between">
                <span class="uppercase text-xs font-bold text-gray-500">{{ $title }}</span>
                <div class="flex items-center gap-3">

                    <div id="actionOptions" class="relative inline-block text-left">
                        <div>
                            <button onclick="$('#dropdownOption').toggle()" type="button"
                                class="flex items-center px-4 py-1 bg-gray-800 text-white text-sm rounded-sm">
                                Options
                                <svg class="-mr-1 h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor"
                                    aria-hidden="true">
                                    <path fill-rule="evenodd"
                                        d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                                        clip-rule="evenodd" />
                                </svg>
                            </button>
                        </div>

                        <div id="dropdownOption"
                            class="hidden absolute right-0 z-10 mt-2 w-60 border rounded-sm bg-white shadow-lg">
                            <div class="py-1 flex flex-col">
                                <a id="updateRecordStatus" data-url="{{ $multiStatusRouteUpdate }}" data-type="1" href="#"
                                    class="block hover:bg-gray-50 px-4 py-2 text-sm">
                                    Publish selected {{ $prefix }}
                                </a>
                                <a id="updateRecordStatus" data-url="{{ $multiStatusRouteUpdate }}" data-type="0" href="#"
                                    class="block hover:bg-gray-50 px-4 py-2 text-sm">
                                    Un-publish selected {{ $prefix }}
                                </a>
                                <a id="deleteSelectedItemButton" href="#" data-url="{{ $delMultiRoute }}"
                                    class="deleteSelectedItemButton hidden hover:bg-gray-50 px-4 py-2 text-sm">
                                    <span id="delInnerTxt" class="delInnerTxt font-medium text-red-500"></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <a href="{{ route('admin.' . $prefix . '.create') }}"
                        class="px-2 py-1 bg-green-500 text-white text-sm hidden rounded-sm">
                        + Add new {{ $singleName }}
                    </a>
                </div>
            </div>
            <div class="p-2">
                {{-- FILTER --}}
                @include('backend.pages.'.$prefix.'.inc.filter')

                {{-- NOTIFICATION --}}
                @include('backend.pages.components.noti')

                {{-- LIST USER --}}
                @include('backend.pages.'.$prefix.'.inc.table')
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('assets/be/js/index/detectClickEvent.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/toggleStatusUpdate.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/elementDefine.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/processCheckboxItem.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/deleteMultiItems.js') }}"></script>
    <script src="{{ asset('assets/be/js/index/updateStatusRecord.js') }}"></script>
@endpush
