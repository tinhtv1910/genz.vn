@extends('backend.layouts.page')

@section('title', $title)

@section('content')

    <div class="px-5 py-2 border-b bg-gray-50">
        <div class="text-2xl font-bold mb-2">{{ $title }}</div>
        <div class="flex items-center gap-3 text-sm">
            <a href="{{ route('admin.dashboard') }}" class="hover:text-blue-500">Dashboard</a>
            <span>/</span>
            <a href="{{ route('admin.' . $prefix . '.index') }}" class="hover:text-blue-500">
                {{ $parentBreadcrumb }}
            </a>
            <span>/</span>
            <span class="font-medium">{{ $title }}</span>
        </div>
    </div>

    <div class="p-5">
        @include('backend.pages.components.noti')

        <div class="rounded-sm border">
            <div class="w-full p-2 bg-gray-50 flex justify-end gap-3">
                <a href="{{ route('admin.orders.invoice', $item->id) }}"
                    class="px-4 py-1 rounded-sm bg-black text-white text-sm flex items-center gap-2">
                    <span>View Invoice</span>
                </a>
                <a href="{{ route('admin.orders.invoice.download', $item->id) }}"
                    class="px-4 py-1 rounded-sm bg-blue-500 text-white text-sm flex items-center gap-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-download" viewBox="0 0 16 16">
                        <path
                            d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5" />
                        <path
                            d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708z" />
                    </svg>
                    <span>
                        Download Invoices
                    </span>
                </a>
            </div>
            <div class="p-2">
                <table class="w-full text-sm">
                    <thead>
                        <tr>
                            <td colspan="2" class="w-1/2 border text-xl font-semibold p-2">
                                Heji.vn
                            </td>
                            <td colspan="2" class="w-1/2 border p-2">
                                <div class="flex flex-col gap-2">
                                    <div class="flex items-center justify-between border-b pb-2 border-dashed">
                                        <div class=""> Invoice ID: </div>
                                        <div class="font-medium">#{{ $item->id }}</div>
                                    </div>
                                    <div class="flex items-center justify-between border-b pb-2 border-dashed">
                                        <div class=""> Today: </div>
                                        <div class="font-medium">{{ date('d-m-Y') }}</div>
                                    </div>
                                    <div class="flex items-center justify-between border-b pb-2 border-dashed">
                                        <div class=""> Store Adress: </div>
                                        <div class="font-medium">Kim giang, Dai kim, Hoang Mai, Ha Noi</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="">
                            <th class="border p-2 font-medium uppercase" colspan="2">Order detail</th>
                            <th class="border p-2 font-medium uppercase" colspan="2">User Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="border p-2">Order ID</td>
                            <td class="border p-2">{{ $item->id }}</td>
                            <td class="border p-2">Name</td>
                            <td class="border p-2">{{ $item->name }}</td>
                        </tr>
                        <tr>
                            <td class="border p-2">Tracking No/ID</td>
                            <td class="border p-2">{{ $item->tracking_no }}</td>
                            <td class="border p-2">Email</td>
                            <td class="border p-2">{{ $item->email ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td class="border p-2">Created Order</td>
                            <td class="border p-2">{{ $item->created_at }}</td>
                            <td class="border p-2">Phone</td>
                            <td class="border p-2">{{ $item->phone }}</td>
                        </tr>
                        <tr>
                            <td class="border p-2">Payment Mode</td>
                            <td class="border p-2">{{ $item->payment_mode }}</td>
                            <td class="border p-2">Address</td>
                            <td class="border p-2">
                                {{ $item->address }} {{ $item->ward->name ?? '' }} {{ $item->district->name ?? '' }}
                                {{ $item->province->name ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td class="border p-2">Transport Mode</td>
                            <td class="border p-2">{{ $item->transport_mode }}</td>
                            <td class="border p-2">Date of Birth</td>
                            <td class="border p-2">{{ $item->dob ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td class="border p-2">Status</td>
                            <td class="border p-2">
                                <span class="" style="color: {{ $item->orderStatus->color }}">
                                    {{ $item->orderStatus->name ?? '-' }}
                                </span>
                            </td>
                            <td class="border p-2">Gender</td>
                            <td class="border p-2">{{ $item->gender == '1' ? 'Male' : 'Female' }}</td>
                        </tr>
                        <tr>
                            <td class="border p-2" colspan="4">
                                <div>Note</div>
                                <textarea rows="3" placeholder="User node" readonly class="w-full border p-2 outline-none resize-none">{{ $item->note }}</textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="mt-5 w-full text-sm">
                    <thead>
                        <tr>
                            <td class="border p-2 font-medium text-xl" colspan="10">Products</td>
                        </tr>
                        <tr>
                            <td class="border p-2 font-medium">#</td>
                            <td class="border p-2 font-medium">Thumb</td>
                            <td class="border p-2 font-medium">Product</td>
                            <td class="border p-2 font-medium">Attribute</td>
                            <td class="border p-2 font-medium">Quantity</td>
                            <td class="border p-2 font-medium">Price</td>
                            <td class="border p-2 font-medium">Total</td>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($item->orderDetails as $order)
                            <tr>
                                <td class="border p-2">{{ $order->id }}</td>
                                <td class="border p-2">
                                    <img src="{{ $order->product->thumb ?? '' }}" class="w-12 h-12" alt="">
                                </td>
                                <td class="border p-2">{{ $order->product->name ?? '' }}</td>
                                <td class="border p-2">{{ $order->attributeValue->name ?? '-' }}
                                    {{ $order->attributeValue->attribute->short_name ?? '' }}</td>
                                <td class="border p-2">{{ $order->quantity }}</td>
                                <td class="border p-2">{{ number_format($order->price, 0, ',', '.') }} VNĐ</td>
                                <td class="border p-2">{{ number_format($order->quantity * $order->price, 0, ',', '.') }}
                                    VNĐ</td>
                            </tr>
                        @empty
                        @endforelse
                        <tr>
                            <td class="border p-2" colspan="10">
                                <div class="w-full flex justify-between items-center">
                                    <span class="text-base font-medium">Total (price include VAT)</span>
                                    <span class="font-medium text-xl">
                                        {{ number_format($item->total_price, 0, ',', '.') }} VNĐ
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="border p-2 text-center" colspan="10">
                                Thank you for shopping <b>HEJI</b>
                            </td>
                        </tr>
                    </tfoot>
                </table>

                <div class="mt-5 text-sm">
                    <div class="mb-3 font-medium">Update order status</div>
                    <div class="mb-1">
                        Current status:
                        <span class="font-medium uppercase text-base" style="color: {{ $item->orderStatus->color }}">
                            {{ $item->orderStatus->name }}
                        </span>
                    </div>
                    @if ($item->status != 6)
                        <form action="{{ route('admin.orders.update', $item->id) }}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="flex items-center flex-wrap justify-end gap-3">
                                <select name="status"
                                    class="w-full border focus:border-blue-500 p-2 outline-none rounded-sm">
                                    <option value="" selected>--Choose status--</option>
                                    @forelse ($status as $statusOrder)
                                        <option value="{{ $statusOrder->id }}"
                                            {{ $item->status == $statusOrder->id ? 'selected' : '' }}>
                                            {{ $statusOrder->name }}
                                        </option>
                                    @empty
                                    @endforelse
                                </select>
                                <button type="submit"
                                    class="bg-blue-500 text-white rounded-sm p-2 hover:shadow-md">Update
                                    status
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
@endpush
