<footer class="py-2 px-5 bg-gray-50 border-t">
    <div class="h-9 items-center flex justify-end text-sm">
        <span class="">Copyright © <a class="text-gray-800 font-semibold"
                href="#" target="_blank">Gen<b class="uppercase text-yellow-400">z</b>.vn </a><script>document.write(new Date().getFullYear())</script></span>
    </div>
</footer>
