<div class="w-full h-full flex flex-col justify-between">
    <div id="sidebar" class="h-[calc(100vh-106px)] px-2 py-5 text-white">
        <div class="flex flex-col">
            <a href="{{ url('admin/dashboard') }}"
                class="p-2 text-sm flex items-center gap-2 rounded-sm hover:bg-gray-500 {{ request()->routeIs('admin.dashboard') ? 'bg-gray-500' : '' }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                    class="bi bi-speedometer" viewBox="0 0 16 16">
                    <path
                        d="M8 2a.5.5 0 0 1 .5.5V4a.5.5 0 0 1-1 0V2.5A.5.5 0 0 1 8 2M3.732 3.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707M2 8a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8m9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5m.754-4.246a.39.39 0 0 0-.527-.02L7.547 7.31A.91.91 0 1 0 8.85 8.569l3.434-4.297a.39.39 0 0 0-.029-.518z">
                    </path>
                    <path fill-rule="evenodd"
                        d="M6.664 15.889A8 8 0 1 1 9.336.11a8 8 0 0 1-2.672 15.78zm-4.665-4.283A11.95 11.95 0 0 1 8 10c2.186 0 4.236.585 6.001 1.606a7 7 0 1 0-12.002 0">
                    </path>
                </svg>
                <span>Dashboard</span>
            </a>
            @foreach (sidebars() as $sidebar)
                @if ($sidebar->type_id)
                    <hr class="my-3">
                @else
                    <div class="group relative">
                        <a href="{{ url('admin/' . $sidebar->slug) }}"
                            class="p-2 text-sm flex items-center gap-2 rounded-sm hover:bg-gray-500 {{ request()->routeIs('admin.' . $sidebar->slug . '.*') ? 'bg-gray-500' : '' }}">
                            {!! $sidebar->icon !!}
                            <div class="w-full flex items-center justify-between">
                                <span>{{ $sidebar->name }}</span>
                                @if (count($sidebar->subMenus))
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                        class="w-3 h-3 group-hover:rotate-90 duration-300" viewBox="0 0 16 16">
                                        <path
                                            d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
                                    </svg>
                                @endif
                            </div>
                        </a>
                        @if (count($sidebar->subMenus))
                            <div class="absolute hidden group-hover:block z-10 shadow-md w-full left-full rounded-sm top-0 bg-gray-800">
                                @foreach ($sidebar->subMenus as $subMenu)
                                    <a href="{{ url('admin/' . $subMenu->slug) }}"
                                        class="p-2 text-sm flex items-center gap-2 rounded-sm hover:bg-gray-500 {{ request()->routeIs('admin.' . $subMenu->slug . '.*') ? 'bg-gray-500' : '' }}">
                                        {!! $subMenu->icon !!}
                                        <span>{{ $subMenu->name }}</span>
                                    </a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="">
        <div onclick="event.preventDefault(); document.getElementById('logoutForm').submit();"
            class="w-full py-2 border-t px-5 text-sm text-yellow-400 cursor-pointer hover:bg-gray-700">
            <div class="flex items-center justify-between h-9">
                <div>Logout</div>
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-box-arrow-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0z" />
                        <path fill-rule="evenodd"
                            d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708z" />
                    </svg>
                </span>
            </div>
        </div>
        <form id="logoutForm" action="{{ route('admin.logout') }}" method="POST" class="hidden">@csrf</form>
    </div>
</div>
