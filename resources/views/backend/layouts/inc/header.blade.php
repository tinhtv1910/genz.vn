<div class="flex items-center">
    <div class="w-1/6 py-2 bg-gray-800 h-auto border-b px-5">
        <div class="h-9 flex items-center">
            <span class="text-xl font-semibold text-white">
                Gen<b class="uppercase text-yellow-400">z</b>.vn
            </span>
        </div>
    </div>
    <div class="w-5/6 bg-gray-50 py-2 px-5 border-b text-sm flex items-center justify-between">
        <div></div>
        <div class="flex items-center py-1 gap-2">
            <div class="">
                <a href="/" target="_blank" class="bg-green-500 hover:-mt-1 durtion-300 text-white flex items-center gap-1 px-2 py-1 rounded-sm">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                        class="bi bi-browser-chrome" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                            d="M16 8a8 8 0 0 1-7.022 7.94l1.902-7.098a3 3 0 0 0 .05-1.492A3 3 0 0 0 10.237 6h5.511A8 8 0 0 1 16 8M0 8a8 8 0 0 0 7.927 8l1.426-5.321a3 3 0 0 1-.723.255 3 3 0 0 1-1.743-.147 3 3 0 0 1-1.043-.7L.633 4.876A8 8 0 0 0 0 8m5.004-.167L1.108 3.936A8.003 8.003 0 0 1 15.418 5H8.066a3 3 0 0 0-1.252.243 2.99 2.99 0 0 0-1.81 2.59M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4" />
                    </svg>
                    <span>Website</span>
                </a>
            </div>
            <div class="flex gap-1 items-center px-2 py-1 rounded-sm bg-gray-800 text-yellow-400">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                    class="bi bi-person-fill" viewBox="0 0 16 16">
                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6" />
                </svg>
                <span class="">{{ auth()->user()->name }}</span>
            </div>
        </div>
    </div>
</div>
