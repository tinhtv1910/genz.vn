<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, viewport-fit=cover, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>@yield('title') | GenZ.vn</title>

    <link rel="shortcut icon" href="" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('assets/css/index.css') }}">

    @stack('style')

    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="">
    <div class="h-screen !overflow-hidden flex flex-col">
        {{-- HEADER --}}
        <section>
            @include('backend.layouts.inc.header')
        </section>

        <section>
            <div class="flex">
                {{-- SIDEBAR --}}
                <div class="w-1/6 bg-gray-800">
                    @include('backend.layouts.inc.sidebar')
                </div>

                {{-- MAIN --}}
                <div class="w-5/6 flex flex-col justify-between">

                    {{-- CONTENT --}}
                    <div class="w-full h-[calc(100vh-106px)] overflow-hidden overflow-y-scroll">
                        @yield('content')
                    </div>

                    {{-- FOOTER --}}
                    <section>
                        @include('backend.layouts.inc.footer')
                    </section>
                </div>
            </div>
        </section>
    </div>

    <script src="{{ asset('vendor/jquery/jquery-3.7.1.min.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    @stack('script')

    <script>
        function stringToSlug(strItem) {
            if (!strItem) {
                return ''
            }
            strItem = strItem.normalize('NFD')
                .replace(/[\u0300-\u036F]/g, '')
                .replace(/đ/g, 'd')
                .replace(/Đ/g, 'd');
            let _url = strItem.toLowerCase();
            _url = _url.replace(/^\s+|\s+$/g, '')
                .replace(/[^a-z0-9 -]/g, '')
                .replace(/\s+/g, '-')
                .replace(/-+/g, '-');
            return _url;
        }
    </script>
</body>

</html>
