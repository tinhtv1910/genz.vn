<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, viewport-fit=cover, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="_token" content="{{ csrf_token() }}" />

    <title>@yield('title') | GenZ.vn</title>
    <link rel="shortcut icon" href="" type="image/x-icon">

    @stack('style')

    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="">

    <section></section>

    <section>
        @yield('content')
    </section>

    <section></section>
    @stack('script')
</body>

</html>
