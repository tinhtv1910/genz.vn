@extends('backend.layouts.admin')

@section('title', 'Login - Admin Panel')

@section('content')
    <div class="w-full h-screen flex items-center justify-center bg-gray-100">
        <div class="w-full bg-white rounded-lg shadow border md:mt-0 sm:max-w-md xl:p-0">
            <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
                    Sign in to your account
                </h1>
                @if (session()->has('err'))
                    <div class="my-2 p-2 rounded bg-red-500 text-sm text-white">
                        <span>{{ session('err') }}</span>
                    </div>
                @endif

                
                @if (session()->has('succ'))
                    <div class="my-2 p-2 rounded bg-green-500 text-sm text-white">
                        <span>{{ session('succ') }}</span>
                    </div>
                @endif

                <form class="space-y-4 md:space-y-6" action="{{ route('admin.login.store') }}" method="POST">
                    @csrf
                    <div>
                        <label for="email" class="block mb-2 text-sm font-medium text-gray-900">Email</label>
                        <input type="email" name="email" id="email" value="{{ old('email') }}" autocomplete="off" 
                            class="bg-white border outline-0 text-gray-900 sm:text-sm rounded-lg focus:border-blue-500 block w-full p-2.5 duration-300"
                            placeholder="name@company.com" required="">
                        @error('email')
                            <span class="text-red-500 text-sm mt-2">{{ $message }}</span>
                        @enderror
                    </div>
                    <div>
                        <label for="password" class="block mb-2 text-sm font-medium text-gray-900">Password</label>
                        <input type="password" name="password" id="password" placeholder="••••••" autocomplete="off"
                            class="bg-white border outline-0 text-gray-900 sm:text-sm rounded-lg focus:border-blue-500 block w-full p-2.5 duration-300"
                            required="">
                        @error('password')
                            <span class="text-red-500 text-sm mt-2">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="flex items-center justify-between">
                        <div class="flex items-start">
                            <div class="flex items-center h-5">
                                <input id="remember" name="remember_me" value="1" aria-describedby="remember"
                                    type="checkbox"
                                    class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300">
                            </div>
                            <div class="ml-3 text-sm">
                                <label for="remember" class="text-gray-500">Remember me</label>
                            </div>
                        </div>
                        <a href="#" class="text-sm font-medium text-primary-600 hover:underline">Forgot password?</a>
                    </div>
                    <button type="submit"
                        class="w-full bg-gray-800 text-white hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">
                        Sign in
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection
