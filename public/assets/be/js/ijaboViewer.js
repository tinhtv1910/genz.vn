$(function() {
    $('input#changeThumbAvatar').ijaboViewer({
        preview: '#image-preview',
        imageShape: '',
        allowedExtensions: ['jpg', 'jpeg', 'png'],
        onErrorShape: function(message) {
            console.log(message);
        },
        onInvalidType: function(message) {
            console.log(message);
        },
    });
});