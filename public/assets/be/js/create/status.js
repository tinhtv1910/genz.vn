$(function() {
    const inputStatus = $('input.status');
    const statusCheckedClass = 'bg-gray-800 text-yellow-400 border-gray-800';

    inputStatus.each(function() {
        if ($(this).prop('checked')) {
            // console.log(this.value);
            $(this).parent().addClass(statusCheckedClass);
        }
    });

    $(document).on('change', 'input.status', function() {
        // console.log($(this).val());
        inputStatus.parent().removeClass(statusCheckedClass);
        if ($(this).prop('checked')) {
            $(this).parent().addClass(statusCheckedClass);
        }
    });
});