$(document).on("click", "#updateRecordStatus", function () {
    if (!listUserIds.length) {
        alert("No items have been selected!");
        return false;
    }
    const _this = $(this);
    const checkedValue = _this.data("type");
    const url = _this.data("url");

    $.ajax({
        url,
        data: {
            listUserIds,
            status: checkedValue,
        },
        type: "POST",
        dataType: "json",
        success: function (response) {
            if (response.code == 200) {
                for (const index of listUserIds) {
                    $(`input#toggleStatusCheckbox${index}`)
                        .val(checkedValue)
                        .prop("checked", Number(checkedValue));
                }
            }
        },
    });
});
