$(function () {
    // MAIN CHECKBOX
    $(document).on("change", "input#checkboxItem", function () {
        const checkedBoxItemLength = $("input#checkboxItem:checked").length;
        mainCheckbox.prop(
            "checked",
            checkedBoxItemLength == checkBoxItemLength
        );

        onUpdateListUserIds();
    });

    // ON CHANGE CHECKBOX ITEM
    $(document).on("change", "input#mainCheckbox", function () {
        if ($(this).prop("checked")) {
            checkBoxItem.prop("checked", true);
        } else {
            checkBoxItem.prop("checked", false);
        }

        onUpdateListUserIds();
    });

    // UPDATE LIST USER ID & DELETE BTN
    function onUpdateListUserIds() {
        listUserIds = [];
        let txt = "";

        $("input#checkboxItem:checked").each(function () {
            listUserIds.push(this.value);
        });

        checkBoxItem.each(function () {
            const _this = $(this);
            if (_this.prop("checked")) {
                _this.closest("tr").addClass("!bg-green-100");
            } else {
                _this.closest("tr").removeClass("!bg-green-100");
            }
        });

        // delInnerTxt
        if (listUserIds.length) {
            deleteSelectedItemButton.show();
            const record = listUserIds.length > 1 ? "items" : "item";
            txt = `Del (${listUserIds.length}) ${record} selected`;
        } else {
            deleteSelectedItemButton.hide();
        }

        deleteSelectedItemButton.children().text(txt);
    }
});
