// UPDATE USER STATUS
$(function() {
    $(document).on('change', '.toggle-checkbox', function() {
        const statusUpdate = this.value == '1' ? '0' : '1';
        const _this = $(this);
        const id = _this.data('id');
        const url = _this.data('url');
        $.ajax({
            type: "POST",
            url,
            data: {
                status: statusUpdate,
                id
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 200) {
                    _this.val(response.data.status);
                }
            }
        });
    });
});