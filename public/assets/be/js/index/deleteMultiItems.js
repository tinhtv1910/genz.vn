// ON DELETE MULTIPLE USER
$(document).on("click", "#deleteSelectedItemButton", function () {
    if (!listUserIds.length) {
        alert("Choose item need to delete.");
        return;
    }
    const _confirm = confirm("Are you sure to delete seleted items?");
    if (!_confirm) return;

    const _this = $(this);
    const url = _this.data("url");

    $.ajax({
        url,
        data: {
            listUserIds,
        },
        type: "POST",
        dataType: "json",
        success: function (response) {
            if (response.code == 200) {
                checkBoxItem.each(function () {
                    if (listUserIds.includes(this.value)) {
                        $(this).closest("tr").remove();
                    }
                });
                deleteSelectedItemButton.children().text("");
                deleteSelectedItemButton.hide();
                listUserIds = [];
            }
        },
    });
});
