// EVENT CLICK OUTSIDE DROPDOWN
$(function() {
    document.addEventListener('click', function(event) {
        const dropdown = document.getElementById('actionOptions');
        if (event.target != dropdown && !dropdown.contains(event.target)) {
            $('#dropdownOption').hide();
        }
    });
});